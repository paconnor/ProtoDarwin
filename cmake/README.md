CMake Tools
===========

This folder contains files used by the CMake build system. They are briefly
documented below:

`CreateVersionHeader.cmake`
---------------------------

This file is a helper script used to generate a (private) version header
containing the git commit hash used when building the code. It is run by the
generated `Makefile` (or whatever build sytem you use). The corresponding logic
is in `src/CMakeLists.txt`.

`FindLibGit2.cmake`
-------------------

This is a [find module](https://cmake.org/cmake/help/latest/command/find_package.html)
for `libgit2`. It was copied with minor changes from Kde's
[`extra-cmake-modules`](https://api.kde.org/frameworks/extra-cmake-modules/html/index.html)
repository. It uses the supporting file `ECMFindModuleHelpers.cmake`.

`CMSSWToolchain.cmake`
----------------------

This file is meant to be used as a
[CMake toolchain](https://cmake.org/cmake/help/latest/manual/cmake-toolchains.7.html)
for CMSSW environments. It contains support for discovering a few libraries
using `scram` and the `find_package` command (Boost, `libgit2`, ROOT).
