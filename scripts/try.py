#!/usr/bin/env python3

import os
import subprocess
import sys
from typing import List

import prefix

MAX_N_EVENTS = 100


def tweak_helper(helper: List[str]) -> None:
    """Tweak the helper from the C++ executables and exits."""

    print("\33[1mtry \33[0m", end="")
    for row in helper:
        if "nNow" in row:
            continue
        if "nSplit" in row:
            continue
        print(row)
    print("")


def main():
    """Implementation of `try` command, relying on homemade `prefix` lib."""

    cmds, args = prefix.preparse(
        sys.argv,
        tutorial=(
            f"Runs the command for {MAX_N_EVENTS} events. Only commands "
            "running over n-tuples may be prefixed, and must natively "
            "output a ROOT file."
        ),
    )

    pref_try = prefix.PrefixCommand(sys.argv[0], cmds)

    if cmds.help:
        tweak_helper(pref_try.helper)

    if cmds.git:
        prefix.git_hash(cmds.exec)

    if cmds.help or cmds.git:
        sys.exit()

    pref_try.parse(args)
    pref_try.prepare_io(multi = False)

    output = str(pref_try.absoutput)

    if os.path.isdir(pref_try.output):
        pref_try.output += f"/0{pref_try.extension}"

    shell_cmd = (
        [pref_try.cmds.exec]
        + pref_try.inputs
        + [output]
        + pref_try.args
    )

    if pref_try.splittable and len(pref_try.inputs) > 0:
        nevents = pref_try.get_entries(pref_try.inputs[0])
        # \todo This will only work for parallelisable commands taking a TTree as input -> we should find a way to change that (will also need to change the helper message).
        pref_try.nSplit = max(nevents // MAX_N_EVENTS, 1)
        shell_cmd += ["-j", str(pref_try.nSplit)]

    print(" ".join(shell_cmd))
    subprocess.run(shell_cmd, check=True)


if __name__ == "__main__":
    try:
        main()
    except Exception as exception:
        prefix.diagnostic(exception)
        sys.exit(1)  # https://tldp.org/LDP/abs/html/exitcodes.html
