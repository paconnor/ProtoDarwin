// SPDX-License-Identifier: GPLv3-or-later
//
// SPDX-FileCopyrightText: Patrick L.S. Connor <patrick.connor@desy.de>
// SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

#pragma once

#include "exceptions.h"
#include "FriendUtils.h"
#include "Looper.h"

#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TTree.h>

#include <any>
#include <array>
#include <filesystem>
#include <map>
#include <memory>
#include <ranges>
#include <source_location>
#include <string>
#include <vector>

namespace Darwin::Tools {

enum BranchMode {
    mandatory  , //!< mounting branch is mandatory
    facultative  //!< mounting branch is facultative
}; //!< parameter when loading branch with \c Flow

using enum BranchMode;

using Slice = std::pair<int, int>; //!< total number of slices (>0) / current slice index (>0)

////////////////////////////////////////////////////////////////////////////////
/// Prints the current slice and the total number of slices.
inline std::ostream& operator<< (std::ostream& Stream, const Darwin::Tools::Slice& slice)
{
    return Stream << slice.second << '/' << slice.first;
}

////////////////////////////////////////////////////////////////////////////////
/// \brief User-friendly handling of input and output n-tuples.
///
/// A flow consists of input and outputs path to n-tuples and of a slice:
/// ~~~cpp
/// DT::Flow flow(steering, inputs);
/// auto tIn = flow.GetInputTree(slice);
/// auto tOut = flow.GetOutputTree(output);
/// auto fOut = flow.GetOutputFile();
/// ~~~
/// The last two lines can be condensed as follows:
/// ~~~cpp
/// auto [fOut, tOut] = flow.GetOutput(output);
/// ~~~
/// The steering is use for the verbosity and the friendship. Setting up branches
/// reduces to the following:
/// ~~~cpp
/// auto recEvent = flow.GetBranchReadOnly<RecEvent>("recEvent");
/// auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets");
/// auto recDijet = flow.GetBranchWriteOnly<RecDijet>("recDijet");
/// ~~~
///
/// Furthermore, as it is frequent to produce histograms along the n-tuple
/// (e.g. control plots), a flow allows to sum on the fly a histogram:
/// ~~~cpp
/// auto hIn = flow.GetInputHist<TH2>("h");
/// auto hIns = flow.GetInputHists("h1", "h2"); // default is TH1
/// ~~~
///
/// \note A one-liner is also possible:
/// ~~~cpp
/// auto hIn = DT::Flow(steering, inputs).GetInputHist("h");
/// ~~~
class Flow {

    int steering; //!< steering from Options, mostly useful for friends

    std::vector<std::filesystem::path> inputs; //!< ROOT files or directories

    std::unique_ptr<ChainSlice> tIn; //!< input chain
    std::shared_ptr<TFile> fOut; //!< output ROOT file
    std::unique_ptr<TTree> tOut; //!< output tree

    std::map<std::string, std::any> branches; //!< pointers to mounted branches

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Get address of branch address in a shared pointer
    template<typename T> //!< branch type
    std::shared_ptr<T*> GetBranch (const std::string& name) const //!< branch name
    try {
        using namespace std;
        if (steering & verbose)
            cout << "Flow: retrieving an existing branch" << endl;
        auto& branch = branches.at(name);
        return any_cast<shared_ptr<T*>>(branch);
    }
    catch (const std::bad_any_cast& e) {
        BOOST_THROW_EXCEPTION(e);
    }

    template<typename T> T * NoBranch (const std::string& name, BranchMode mode)
    {
        if (mode == facultative)
            return nullptr;

        namespace DE = Darwin::Exceptions;
        std::string what = name + " branch could not be found";
        BOOST_THROW_EXCEPTION( DE::BadInput(what.c_str(), *tIn) );
    }

public:
    ////////////////////////////////////////////////////////////////////////////
    /// \brief Returns a list of all active branches, formatted as a string.
    inline std::string DumpActiveBranches () const
    {
        auto keys = branches | std::views::keys;
        return std::accumulate(keys.begin(), keys.end(), std::string(),
                [](const std::string& str, const std::string& branch_name) {
                    return str + ' ' + branch_name;
                });
    }

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Constructor.
    ///
    /// Determines the command in which Flow is called.
    Flow (int = none, //!< steering bitfield
          const std::vector<std::filesystem::path>& = {} //!< ROOT files or directories
         );

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Destructor.
    ///
    /// Saves the output tree (if declared) to the output file (if open).
    ~Flow ();

    ////////////////////////////////////////////////////////////////////////////
    /// Load chain from a list of files.
    ChainSlice * GetInputTree
            (const Slice = {1, 0}, //!< number and index of slice
             const std::string& = "events"); //!< internal path to `TTree`

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Load ROOT histograms from a list of files.
    ///
    /// In many cases, this spares from running an intermediate `hadd` (which
    /// can be very slow if there is a `TTree` in the same ROOT file).
    template<typename THX = TH1, size_t N>
    std::array<std::unique_ptr<THX>, N> GetInputHists
            (const std::array<std::string, N>& names = {} //!< path to ROOT histograms
            )
    {
        using namespace std;
        namespace fs = filesystem;

        if (inputs.size() == 0)
            BOOST_THROW_EXCEPTION( runtime_error("Empty input list") );

        array<unique_ptr<THX>, N> sums;
        for (const fs::path& input: inputs) {
            auto fIn = make_unique<TFile>(input.c_str(), "READ");
            for (size_t i = 0; i < N; ++i) {
                const string& name = names[i];
                unique_ptr<THX> h(fIn->Get<THX>(name.c_str()));
                if (!h) {
                    namespace DE = Darwin::Exceptions;
                    BOOST_THROW_EXCEPTION(
                            DE::BadInput(Form("`%s` cannot be found in (one of) the "
                                              " file(s).", name.c_str()), fIn));
                }
                if (sums[i])
                    sums[i]->Add(h.get());
                else {
                    sums[i] = std::move(h);
                    sums[i]->SetDirectory(nullptr);
                }
            }
        }
        return sums;
    }

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Load ROOT histograms from a list of files.
    ///
    /// In many cases, this spares from running an intermediate `hadd` (which
    /// can be very slow if there is a `TTree` in the same ROOT file).
    template<typename THX = TH1, typename... Args>
    auto GetInputHists (const Args... args)
    {
        constexpr const size_t N = sizeof...(args);
        std::array<std::string, N> names {{ args... }};
        return GetInputHists<THX, N>(names);
    }

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Load a single ROOT histogram from a list of files.
    ///
    /// \todo Can we rather use a node handle to extract the unique pointer?
    template<typename THX = TH1>
    std::unique_ptr<THX> GetInputHist
            (const std::string& name) //!< internal path to ROOT histogram
    {
        auto hists = GetInputHists<THX,1>({name});
        THX * hist = hists.front().release();
        return std::unique_ptr<THX>(hist);
    }

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Create an output `TTree` object
    ///
    /// If the `TTree` already exists, either clone the input `TChain` or define
    /// the input `TChain` as a friend of the new `TTree`. Else a brand new
    /// `TTree` is stored.
    ///
    /// The title of the `TTree` is used to store (retrieve) the name of the
    /// present (previous) command. When loading a tree with friends, one can
    /// refer to old versions of the branches by prefixing them with the
    /// corresponding command name.
    ///
    /// \todo `TTree::BuildIndex()` to preserve the association despite skipped events?
    TTree * GetOutputTree (std::shared_ptr<TFile> = {}, //!< output file
            const std::source_location = std::source_location::current()); //!< upstream function

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Create an output `TTree` object
    TTree * GetOutputTree (const std::filesystem::path&, //!< output directory
            const std::source_location = std::source_location::current()); //!< upstream function

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Set the output file where the output `TTree` should be saved
    inline void SetOutputFile (std::shared_ptr<TFile> fOut) { this->fOut = fOut; }

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Get a raw pointer to the output file
    inline TFile * GetOutputFile () { return fOut.get(); }

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Get both the output file and the output tree in one go
    std::pair<TFile *, TTree *> GetOutput (const std::filesystem::path&, //!< output directory
            const std::source_location = std::source_location::current()); //!< upstream function

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Wrapper to initialise read-only branches
    ///
    /// The existence of the branch is checked before setting the address.
    ///
    /// In practice, loading a branch whose contents should be accessed but not
    /// modified reduces to a single line:
    /// ~~~{cpp}
    /// auto recEvent = flow.GetBranchReadOnly<RecEvent>("recEvent");
    /// ~~~
    template<typename T> //!< branch type (e.g. `RecEvent`, `vector<RecJet>`)
    T * GetBranchReadOnly (
                const std::string& name, //!< branch name
                BranchMode mode = mandatory) //!< branch may not exist
    {
        using namespace std;

        if (!tIn)
            BOOST_THROW_EXCEPTION( invalid_argument("`GetInputTree()` should "
                            "be called before declaring a read-only branch") );

        namespace DE = Darwin::Exceptions;

        shared_ptr<T*> t;

        if (branches.contains(name))
            t = GetBranch<T>(name);
        else {
            t = make_shared<T*>();
            if (steering & verbose)
                cout << "Flow: loading branch `" << name << "`" << endl;

            if (tIn->GetBranch(name.c_str()) == nullptr)
                return NoBranch<T>(name, mode);

            int err = tIn->SetBranchAddress(name.c_str(), t.get());
            if (steering & verbose)
                cout << "Flow: `TTree::SetBranchAddress()` returned " << to_string(err)
                     << " (check `TTree::ESetBranchAddressStatus` for the meaning)."
                     << endl;
            if (err < 0) {
                string what = "`"s + name + "` branch could not be set. "s;
                if (mode == facultative) {
                    if (steering & verbose)
                        cout << orange << "Flow: " << what << def << endl;
                    return nullptr;
                }
                BOOST_THROW_EXCEPTION( DE::BadInput(what.c_str(), *tIn) );
            }
            branches.insert({name, t});
        }
        return *t;
    }

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Wrapper to initialise write-only branches
    ///
    /// In practice, setting up a new branch in a n-tuple reduces to a single
    /// line:
    /// ~~~{cpp}
    /// auto recEvent = flow.GetBranchWriteOnly<RecEvent>("recEvent");
    /// ~~~
    template<typename T> //!< branch type (e.g. `RecEvent`, `vector<RecJet>`)
    T * GetBranchWriteOnly (const std::string& name) //!< branch name
    {
        using namespace std;

        if (!tOut)
            BOOST_THROW_EXCEPTION( invalid_argument("`GetOutputTree()` should "
                                                    "be called before") );

        shared_ptr<T*> t;

        if (branches.contains(name))
            t = GetBranch<T>(name);
        else
            t = make_shared<T*>();

        if (steering & verbose)
            cout << "Flow: setting up new branch for `" << name << "`" << endl;
        if (tOut->Branch(name.c_str(), t.get()) == nullptr) {
            namespace DE = Darwin::Exceptions;
            string what = name + " branch could not be set up";
            BOOST_THROW_EXCEPTION( DE::BadInput(what.c_str(), *tOut) );
        }

        if (!branches.contains(name))
            branches.insert({name, t});
        return *t;
    }

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Wrapper to initialise read-write branches
    ///
    /// In practice, setting up a branch to modify its contents reduces to a
    /// single line:
    /// ~~~{cpp}
    /// auto recEvent = flow.GetBranchReadWrite<RecEvent>("recEvent");
    /// ~~~
    template<typename T> //!< branch type (e.g. `RecEvent`, `vector<RecJet>`)
    T * GetBranchReadWrite (const std::string& name, //!< branch name
                            BranchMode mode = mandatory) //!< branch may not exist
    {
        using namespace std;

        if (!tIn)
            BOOST_THROW_EXCEPTION( invalid_argument("`GetInputTree()` should "
                            "be called before declaring a read-write branch") );

        if (!tOut)
            BOOST_THROW_EXCEPTION( invalid_argument("`GetOutputTree()` should "
                            "be called before declaring a read-write branch") );

        shared_ptr<T*> t;

        if (branches.contains(name))
            t = GetBranch<T>(name);
        else {
            if (GetBranchReadOnly<T>(name, mode) == nullptr)
                return NoBranch<T>(name, mode);
            if (steering & Friend)
                GetBranchWriteOnly<T>(name);
            t = any_cast<shared_ptr<T*>>(branches[name]);
        }
        return *t;
    }
};

} // namespace Darwin::Tools

using Darwin::Tools::operator<<;
