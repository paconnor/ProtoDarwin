/// SPDX-License-Idendifier: GPL-3.0-or-later
/// SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

#pragma once

/**
 * \brief Utilities enabling better support for friend chains in a \c TTree.
 */

#include <TChain.h>
#include <TFile.h>
#include <TFriendElement.h>
#include <TNotifyLink.h>
#include <TObjString.h>

namespace Darwin::Tools {

/**
 * \brief Wrapper around \c TChain that restricts available entries to a
 *        subset.
 *
 * This class acts like a \c TChain, with the additional feature of limiting
 * the available entries to a subset of the original. The first entry that will
 * be used is set with \ref SetBeginEntry, and the last one with
 * \ref SetEndEntry. Once set, a normal iteration through the chain (with
 * \ref SetEntry or similar) will be restricted to entries in [begin, end).
 *
 * In essence, a \c ChainSlice behaves like iterating on a normal chain from
 * \ref GetBegin (inclusive) to \ref GetEnd (exclusive).
 *
 * \note Adding friends to a \c ChainSlice is untested.
 * \bugs Calling LoadTree() directly will not work as expected.
 */
class ChainSlice : public TChain
{
    Long64_t fBegin; ///< The first entry to use.
    Long64_t fEnd;   ///< Past the last entry to use, or -1 to go till the end.

    /**
     * Returns the last entry to load, taking negative fEnd into account.
     */
    auto GetLast () const
    {
        return fEnd >= 0 ? std::min(fEnd, TChain::GetEntries())
                         : TChain::GetEntries();
    }

public:
    // Disable copies.
    ChainSlice (const ChainSlice&) = delete;

    /**
     * \brief Creates a \c ChainSlice.
     *
     * The first and last entries can optionally be provided.
     */
    ChainSlice (Long64_t begin = 0, Long64_t end = -1)
        : fBegin(begin)
        , fEnd(end)
    {
        // Force creation of the friends list. This controls friend handling in
        // TChain::LoadTree, *also for the friends of the loaded TTree*. I
        // consider this a bug in ROOT.
        fFriends = new TList();
    }

    /**
     * \brief Creates a \c ChainSlice.
     *
     * The first and last entries can optionally be provided.
     * See the \c TChain constructor for other arguments.
     */
    ChainSlice (const char * name,
                Long64_t begin = 0,
                Long64_t end = -1,
                const char * title = "")
        : TChain(name, title)
        , fBegin(begin)
        , fEnd(end)
    {
        // Force creation of the friends list. This controls friend handling in
        // TChain::LoadTree, *also for the friends of the loaded TTree*. I
        // consider this a bug in ROOT.
        fFriends = new TList();
    }

    // Only to have something in the source file for linking purpose.
    virtual ~ChainSlice ();

    /**
     * \brief Returns the index of the first entry that will be used.
     */
    Long64_t GetBegin () const { return fBegin; }

    /**
     * \brief Returns the index past the last entry that will be used.
     */
    Long64_t GetEnd () const { return fEnd; }

    /**
     * \brief Changes the first entry to be loaded.
     *
     * The currently loaded entry is not modified.
     */
    void SetBegin (Long64_t begin) { fBegin = begin; }

    /**
     * \brief Changes the last entry to be loaded.
     *
     * The currently loaded entry is not modified.
     */
    void SetEnd (Long64_t end) { fEnd = end; }

    /**
     * \brief Return the total number of entries in the chain, taking the
     *        restrictions set by \ref GetBegin and \ref GetEnd into account.
     */
    Long64_t GetEntries () const override { return GetLast() - GetBegin(); }

    /**
     * \brief Return the total number of entries in the chain, taking the
     *        restrictions set by begin and end into account.
     *
     * Synomym for \ref GetEntries.
     */
    Long64_t GetEntriesFast () const override { return GetEntries(); }

    /**
     * \brief Get entry from the chain to memory.
     *
     * \todo description
     *
     * See \c TChain::GetEntry for details.
     */
    Int_t GetEntry (Long64_t entry, Int_t getall = 0) override
    { return TChain::GetEntry(entry + GetBegin(), getall); }

    /**
     * \brief Number of the currently-loaded entry.
     *
     * This override takes \ref GetBegin into account.
     */
    Long64_t GetReadEntry () const override { return TChain::GetReadEntry() - GetBegin(); }

    /**
     * \brief Number of the currently-loaded entry.
     *
     * This override takes \ref GetBegin into account.
     */
    Long64_t GetReadEvent () const override { return GetReadEntry(); }

    /**
     * \brief Loads the requested entry, called from a friend tree.
     *
     * This override takes \ref GetBegin into account.
     */
    Long64_t LoadTreeFriend (Long64_t entry, TTree * parent) override
    { return TChain::LoadTreeFriend(entry + GetBegin(), parent); }

    /**
     * \brief Prints information about the current tree in the chain.
     */
    void Print (Option_t *) const override;

    ClassDefOverride(ChainSlice, 1)
};

/**
 * \brief A \c TFriendElement that remembers the list of files and slicing of a
 *        \ref ChainSlice.
 */
class SlicedFriendElement : public TFriendElement
{
    Long64_t fBegin =  0; ///< Begin parameter of the ChainSlice.
    Long64_t fEnd   = -1; ///< End parameter of the ChainSlice.

    /// \brief The list of trees in the chain.
    ///
    /// The list contains strings in a format suitable for
    /// \c TChain::AddFileInfoList. We keep track of this separately from
    /// the chain because \c fChain isn't saved. This list does get saved.
    TList * fTrees = nullptr;

    ChainSlice * fChain = nullptr;            ///<! The loaded chain.
    bool fOwnsChain = false;                  ///<! Whether we own the tree.
    TNotifyLink<SlicedFriendElement> fNotify; ///<! See comment in GetTree().

public:
    SlicedFriendElement (): fNotify(this) {}
    SlicedFriendElement (TTree * parent,
                         ChainSlice * friendChain,
                         const char * alias);
    virtual ~SlicedFriendElement();

    static SlicedFriendElement * AddTo (TTree * tree,
                                        ChainSlice * chain,
                                        const char * alias);

    TTree * GetTree () override;
    TFile * GetFile () override;

    /**
     * \brief Returns the list of trees in the associated chain.
     *
     * The list contains strings in a format suitable for
     * \c TChain::AddFileInfoList.
     */
    TList * GetListOfTrees () const { return fTrees; }

    /// Index of the first entry to use in the chain.
    Long64_t GetBegin () const { return fBegin; }

    /// Index of a past-the-end entry to use in the chain.
    Long64_t GetEnd () const { return fEnd; }

    bool Notify () override;
    void Print (Option_t * opt = 0) const override;

    ClassDefOverride(SlicedFriendElement, 1)
};

} // namespace Darwin::Tools
