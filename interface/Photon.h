#pragma once

// STD
#include <cstdint>
#include <ios>

// Darwin
#include <GenericObject.h>

// ROOT
#include <TString.h>

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Generic generator-level photon
class GenPhoton : public GenericObject {

    std::string_view scale_group () const override { return ScaleVar; }
    std::string_view weight_group () const override { return WeightVar; }

public:
    static inline const char * const ScaleVar = "GenPhotonScales",
                             * const WeightVar = "GenPhotonWgts";

    bool zAncestor = false, //!< Z boson among the particle mothers
         prompt = false; //!< Originates directly from the matrix element

    GenPhoton () = default;
    virtual ~GenPhoton() = default;
};

////////////////////////////////////////////////////////////////////////////////
/// Generic detector-level photon
class RecPhoton : public GenPhoton {

    std::string_view scale_group () const override { return ScaleVar; }
    std::string_view weight_group () const override { return WeightVar; }

public:
    static inline const char * const ScaleVar = "RecPhotonScales",
                             * const WeightVar = "RecPhotonWgts";

    std::uint32_t selectors = 0; //!< Identification cuts

    RecPhoton () = default;
    virtual ~RecPhoton() = default;
};

} // end of Darwin::Physics namespace

inline std::ostream& operator<< (std::ostream& s, const Darwin::Physics::GenPhoton& photon)
{
    return s << photon.CorrP4() << ' ' << photon.zAncestor << ' ' << photon.prompt;
}

inline std::ostream& operator<< (std::ostream& s, const Darwin::Physics::RecPhoton& photon)
{
    return s << photon.CorrP4() << " 0x" << std::hex << photon.selectors << std::dec;
}
