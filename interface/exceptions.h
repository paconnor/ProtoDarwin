#ifndef DARWIN_EXCEPTIONS_H
#define DARWIN_EXCEPTIONS_H
#include <stdexcept>
#include <string>
#include <memory>
#include <numeric>

#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include "colours.h"
#include "MetaInfo.h"
#include "Options.h"

namespace Darwin::Exceptions {

////////////////////////////////////////////////////////////////////////////////
/// Solution found on Stackoverflow to intercept the output of `printf` 
/// (which is still used by ROOT in several places, e.g. `TTree::Show()`).
///
/// https://stackoverflow.com/questions/5911147/how-to-redirect-printf-output-back-into-code
///
/// \note This function only collects the first 16kB printed by the intercepted
///       function. If the output is longer, the return value is truncated.
/// \bug This function will deadlock if the intercepted function produces more
///      output than a system-dependent limit (the size of a pipe buffer, which
///      can be as low as 4kB on some systems).
/// \bug This function leaks file descriptors if the intercepted function
///      throws.
inline std::string intercept_printf (std::function<void()> const lambda = []() { printf(__func__); })
{
    char buffer[16384] = {0};
    int out_pipe[2];

    // Get rid of anything still present in the stdout buffer. If there were
    // something left in the buffer, we would "intercept" it.
    fflush(stdout);

    int saved_stdout = dup(STDOUT_FILENO);  /* save stdout for display later */

    if (pipe(out_pipe) != 0) // make a pipe
        return "[`printf()` couldn't be intercepted]";

    dup2(out_pipe[1], STDOUT_FILENO);   /* redirect stdout to the pipe */
    close(out_pipe[1]);

    lambda();
    fflush(stdout);

    read(out_pipe[0], buffer, sizeof(buffer)-1); /* read from pipe into buffer */
    close(out_pipe[0]);

    dup2(saved_stdout, STDOUT_FILENO);  /* reconnect stdout for testing */

    return buffer;
}

////////////////////////////////////////////////////////////////////////////////
/// Generic exception for problematic event (during event loop).
struct AnomalousEvent : public std::runtime_error {

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor for bad input tree
    template<typename TTreePtr>
    AnomalousEvent (const char * error, //!< error message
                    const TTreePtr& tree //!< pointer to `TTree`
                   ) : runtime_error(
                           intercept_printf([error,&tree]() {
                               printf("%s%s%s\n", bold, error, normal);
                               tree->Show();
                           })
                       )
    { }

    // TODO: alternative constructor for `example01` (initialise with any object and "dump" it?)
};

////////////////////////////////////////////////////////////////////////////////
/// Generic exception for ill-defined input (before the event loop).
struct BadInput : public std::invalid_argument {

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor for bad meta information
    BadInput (const char * error, //!< error message
              std::function<void()> helper //!< helper
             ) : invalid_argument(
                     intercept_printf([error,&helper]() {
                         printf("%s%s%s\n", bold, error, normal);
                         helper();
                     })
                 )
    { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor for bad meta information
    BadInput (const char * error, //!< error message
              const Darwin::Tools::UserInfo& userinfo //!< access to userinfo
             ) : BadInput(error, [&userinfo](){ userinfo.ls(); }) { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor for `TObject` (in particular, `TChain` and `TH1`)
    BadInput (const char * error,
              const TObject& object //!< access to ROOT object
             ) : BadInput(error, [&object](){ object.Print(); }) { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor for bad `TDirectory` or `TFile`
    BadInput (const char * error,
              const TDirectory& dir //!< access to TFile or TDirectory
             ) : BadInput(error, [&dir](){ dir.ls(); }) { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Specialisation for shared pointers
    template<typename T>
    BadInput (const char * error, const std::shared_ptr<T>& ptr
             ) : BadInput(error, *ptr) { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Specialisation for unique pointers
    template<typename T>
    BadInput (const char * error, const std::unique_ptr<T>& ptr
             ) : BadInput(error, *ptr) { }
};

////////////////////////////////////////////////////////////////////////////////
/// Standard exception diagnostic, including the expanded command, software
/// versions, and link to the documentation.
inline void Diagnostic (const boost::exception& e) //!< argument of `catch` block
{
    using namespace std;
    namespace DT = Darwin::Tools;
    cerr << red << boost::diagnostic_information(e);
    if (!DT::Options::full_cmd.empty())
        cerr << "To reproduce error, you may run:\n"
             << highlight << DT::Options::full_cmd << def << '\n';
    cerr << def;
}

}
#endif
