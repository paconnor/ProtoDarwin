#pragma once

#include <filesystem>
#include <map>
#include <optional>
#include <regex>
#include <set>
#include <string>

#include "UserInfo.h"
#include "Variation.h"

#ifndef DARWIN_GIT_REPO
#  ifndef DARWIN
#    error "Provide the location of the Git repository at compile time."
#  else
#    define DARWIN_GIT_REPO DARWIN
#  endif
#endif

struct git_repository;

namespace Darwin::Tools {

static std::ostream dev_null(nullptr); //!< to redirect output stream to nowhere

////////////////////////////////////////////////////////////////////////////////
/// \brief Generic meta-information for n-tuple (including speficities to Darwin).
///
/// Wrapper for `TList` information typically stored in `TTree::GetUserInfo()`.
/// The list includes information such as:
///  - a list of generic flags, e.g. the R parameter and jet algo (`flags`)
///  - some minimal software version information (`git`),
///  - the list of corrections applied so far (`corrections`),
///  - the successive commands run so far (`history`)
///  - and anything else that is possibly relevant (the format is flexible).
/// The information stored in the list can be converted into a config file
/// to reproduce identical results.
///
/// To initialise the meta information in a new `TTree`, a config with flags
/// is required:
/// ~~~cpp
/// MetaInfo metainfo(tree, config);
/// ~~~
/// To modify the meta information of an existing `TTree`, only the tree is 
/// necessary, but the config may be compared to the existing meta information:
/// ~~~cpp
/// MetaInfo metainfo(tree);
/// metainfo.Check(config);
/// ~~~
/// This is typically useful for reproducibility purposes, in the case where
/// the `config` was extracted from another `TTree` at a more advanced stage.
///
/// A typical use case is to determine generic flags for the data processing:
/// ~~~cpp
/// bool isMC = metainfo.Get<bool>("flags", "isMC");
/// int year = metainfo.Get<bool>("flags", "year");
/// ~~~
/// 
/// The `metainfo` can then be used like any other `UserInfo` instance.
/// Although one is entirely free to store any sort of information, it is important
/// to keep in mind the minimal structure of the file (see above) and respect it
/// as much as possible. For instance, to indicate an additional correction, the 
/// preferred way is the following:
/// ~~~cpp
/// metainfo.Set<string>("corrections", "nameOfMyCorrection", "value");
/// ~~~
class MetaInfo : public UserInfo {

    git_repository * repo; //!< pointer to local Git repo
    mutable int git_error; //!< C-style error code from Git operations 
                           //!< (see [source code](https://github.com/libgit2/libgit2/blob/v1.0.1/include/git2/errors.h))
    char sha[16]; //!< commit SHA at running time
    const std::filesystem::path origin; //!< path to local repo determined at compilation time

    ////////////////////////////////////////////////////////////////////////////////
    /// Function to check if a file is ignored.
    ///
    /// \return EXIT_SUCCESS or EXIT_FAILURE
    static int get_one_status (const char *, //!< path to file
                       unsigned int, //!< status (necessary for `git_status_foreach` but unused)
                       void * //!< pointer to `git_repository`
                       );

    ////////////////////////////////////////////////////////////////////////////////
    /// Inits and opens git repo, sets `sha` to the value at running time.
    void GitInit ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Mimicks `git status`.
    ///
    /// Adapted from 
    /// [this example](https://libgit2.org/libgit2/ex/v1.0.1/status.html).
    /// See also 
    /// [the documentation](https://libgit2.org/libgit2/#HEAD/group/status).
    void PrintStatus () const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Function to check possibly uncommitted changes per file. It calls internally
    /// `MetaInfo::get_one_status` with a foreach loop.
    ///
    /// \return `true` if all changes have been committed
    bool AllChangesCommitted () const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Fills in git information in the MetaInfo.
    void SetupGit (bool);

    ////////////////////////////////////////////////////////////////////////////////
    /// Fills in software version information in the MetaInfo.
    void SetupVersions ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Update list with present git information. The `flags` and `git` blocks are
    /// expected to be found, otherwise it crashes smoothly. Then it updates the
    /// Git information.
    void UpdateList (const bool //!< should assume completeness (typically `false` for event loops)
                    ) const;


    ////////////////////////////////////////////////////////////////////////////////
    /// Used internally to set the git repo location (last parameter).
    MetaInfo (bool, const char*);

    ////////////////////////////////////////////////////////////////////////////////
    /// Used internally to set the git repo location (last parameter).
    MetaInfo (TTree *, const boost::property_tree::ptree&, bool,
              const char*);

    ////////////////////////////////////////////////////////////////////////////////
    /// Used internally to set the git repo location (last parameter).
    MetaInfo (TTree *, bool, const char*);

    ////////////////////////////////////////////////////////////////////////////////
    /// Used internally to set the git repo location (last parameter).
    MetaInfo (TList *, bool, const char*);
public:

    ////////////////////////////////////////////////////////////////////////////////
    /// If you use this, you're supposed to know what you are doing.
    struct IKnowWhatIAmDoing {};

    ////////////////////////////////////////////////////////////////////////////////
    /// Constsructor for first call ever. This creates a minimal MetaInfo.
    ///
    /// This is a low-level constructor and the produced MetaInfo lacks some
    /// information:
    /// * The preseed is chosen randomly.
    /// * The caller is responsible for populating "flags".
    /// * No command history is recorded.
    /// * git.complete is set to false.
    /// Do not use this constructor.
    MetaInfo (const IKnowWhatIAmDoing&, bool git_init = true)
        : MetaInfo(git_init, DARWIN_GIT_REPO)
    {}

    ////////////////////////////////////////////////////////////////////////////////
    /// Constsructor for first call ever, where the second parameter should be
    /// extracted from the config file or from the command line.
    ///
    /// The preseed is taken from the config file *or* generated on the fly in a
    /// non-reproducible way. Use `getMetaInfo` for see how to force the seed
    /// to a value from the config file.
    MetaInfo (TTree *t, //!< `TTree` whose `UserInfo` will be populated
              const boost::property_tree::ptree& config, //!< input config (must include flags)
              bool git_init = true
             )
        : MetaInfo(t, config, git_init, DARWIN_GIT_REPO)
    {}

    ////////////////////////////////////////////////////////////////////////////////
    /// Constsructor for call of existing n-tuple. By default, the `git.complete`
    /// flag will be reset to `false`. It should be set to `true` after the event
    /// loop in the executable.
    MetaInfo (TTree *t, //!< `TTree` whose `UserInfo` will be modified
              bool git_init = true //!< flag to init git repo
             )
        : MetaInfo(t, git_init, DARWIN_GIT_REPO)
    {}

    ////////////////////////////////////////////////////////////////////////////////
    /// Constsructor for direct call to a list (typically from a projection). By
    /// default, the `git.complete` will be kept as it is.
    MetaInfo (TList *l, //!< `TList` that will be modified
              bool git_init = true //!< flag to init git repo
             )
        : MetaInfo(l, git_init, DARWIN_GIT_REPO)
    {}

    ////////////////////////////////////////////////////////////////////////////////
    /// Generic constsructor for smart pointers
    template<typename Ptr, typename ...Args>
    MetaInfo (const Ptr& t, Args... args)
        : MetaInfo(&*t, args...) { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor (releases memory & shuts down Git).
    ~MetaInfo ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Compare the meta information with the config given in argument. Either 
    /// warnings or errors are thrown according to the severity of the inconsistencies.
    /// It also completes the records of successive commands.
    void Check (const boost::property_tree::ptree&) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// \return string with libgit2 version.
    static std::string libgit2_version ();

    static std::map<std::string, std::string> versions; //!< software version

    ////////////////////////////////////////////////////////////////////////////////
    /// Generate the seed in a reproducible way:
    /// \f[
    ///    s = h + (k+1) \times b
    /// \f]
    /// where
    /// - $h$ is hard-coded in the program and should be different for every call 
    ///   of the functor,
    /// - $k$ corresponds to the (incremented) slice index,
    /// - and $b$ should be different for every sample (a.k.a. *preseed*).
    template<unsigned h //!< hard-coded component
    > unsigned Seed (std::pair<unsigned short, unsigned short> slice = {1,0} //!< variable part
                    ) const
    {
        unsigned b = Get<int>("preseed");
        unsigned k = slice.second;
        return h + (k+1) * b;
    }

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Define a family of uncertainties
    /// 
    /// This function offers a unique proxy to define a large number of similar
    /// uncertainties, including various sources, correlation bits, up and down
    /// variations, and replicas. For instance, for various JES uncertainties:
    /// ~~~cpp
    /// auto variations = metainfo.AddVars(RecJet::ScaleVar, {"FlavourQCD", /* ... */});
    /// ~~~
    /// or for a hundred replicas (e.g. when the seed of a generator is changed):
    /// ~~~cpp
    /// auto variations = metainfo.AddVars(GenEvent::WeightVar, {"Replica"}, 100, replicas);
    /// ~~~
    /// or for the impact of a binned correction caused by physics objects on
    /// the whole event (where 20 is the number of bins):
    /// ~~~cpp
    /// auto variations = metainfo.AddVars(RecEvent::WeightVar, {"Prefiring"}, 20, symmetric | replicas);
    /// ~~~
    /// or for to express partial correlations:
    /// ~~~cpp
    /// auto variations = metainfo.AddVars(RecJet::WeightVar, {"bSFstat"}, 3, symmetric | bits);
    /// ~~~
    std::set<Darwin::Physics::Variation> AddVars
        (const std::string&, //!< group
         const std::vector<std::string>&, //!< names
         const int = Darwin::Physics::symmetric, //!< variation type
         const int = 1 //!< number of members (replicas or correlation bit)
         );

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Retrieve a series of uncertainties
    std::set<Darwin::Physics::Variation> GetVars
        (const std::string&, //!< group
         const std::string& = "" //!< regex search
        ) const;

    ////////////////////////////////////////////////////////////////////////////
    /// \brief Retrieve all variations, including the nominal one.
    std::set<Darwin::Physics::Variation> GetVars () const;
};

} // end of namespace
