#pragma once

// C
#include <cmath>

// STD
#include <ostream>
#include <string_view>
#include <vector>

// ROOT
#include <Math/Vector4D.h>

// Darwin
#include <Variation.h>
#include <Weight.h>

namespace Darwin::Physics {

using FourVector = ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<float>>;

////////////////////////////////////////////////////////////////////////////////
/// Generic physics entity, either an object directly reconstructed in the
/// detector, or a composite system made of such objects. The existence of such
/// a class is motivated by the need to define a way to access variations in a
/// common way for simple and composite objects with `Darwin::Physics::Variation`.
class AbstractGenericObject {

public:
    AbstractGenericObject () = default;
    virtual ~AbstractGenericObject () = default;

    virtual FourVector CorrP4 (const Variation&) const = 0;
    virtual float      CorrPt (const Variation&) const = 0;
    virtual double     Weight (const Variation&) const = 0;
};

////////////////////////////////////////////////////////////////////////////////
/// Generic physics object, such as a jet or a stable particle.
///
/// The four-momentum p4 does not include the energy calibration.
/// The `scales` vector typically contains the JES or Rochester corrections.
/// The `weights` include a value and a (de)correlation bit.
class GenericObject : public AbstractGenericObject {
protected:
    virtual std::string_view scale_group  () const = 0;
    virtual std::string_view weight_group () const = 0;

    GenericObject () = default;
    virtual ~GenericObject () = default;

public:
    FourVector p4; //!< raw four-momentum directly after reconstruction
    std::vector<float> scales = {1.}; //!< energy scale corrections and variations
    Weights weights = {{1.,0}}; //!< object weights

    inline FourVector CorrP4 (size_t i = 0) const { return p4      * scales.at(i); } //!< corrected 4-vector
    inline float      CorrPt (size_t i = 0) const { return p4.Pt() * scales.at(i); } //!< corrected transverse momentum
    inline float Rapidity (const Variation& = nominal) const { return       p4.Rapidity() ; }
    inline float AbsRap   (const Variation& = nominal) const { return std::abs(Rapidity()); } //!< absolute rapidity

    inline FourVector CorrP4 (const Variation& v) const final //!< corrected 4-vector
    { return GenericObject::CorrP4(v.Group() == scale_group() ? v.Index() : 0); }

    inline float CorrPt (const Variation& v) const final //!< corrected transverse momentum
    { return GenericObject::CorrPt(v.Group() == scale_group() ? v.Index() : 0); }

    inline double Weight (const Variation& v) const final //!< weight
    {
        if (v.Group() == weight_group()) {
            const Darwin::Physics::Weight& w = weights.at(v.Index());
            if (w.i == v.Bit()) return w;
        }
        return weights.front();
    }
};

inline bool operator== (const GenericObject& l, const GenericObject& r)
{
    return l.p4 == r.p4 && l.scales == r.scales && l.weights == r.weights;
}

inline bool operator< (const GenericObject& l, const GenericObject& r)
{
    return l.CorrPt() < r.CorrPt();
}

inline bool operator> (const GenericObject& l, const GenericObject& r)
{
    return l.CorrPt() > r.CorrPt();
}

} // end of Darwin::Physics namespace

#if 0
inline std::ostream& operator<< (std::ostream& s, const Darwin::Physics::FourVector& p4)
{
    return s << '(' << p4.Pt() << ',' << p4.Eta() << ',' << p4.Phi() << ',' << p4.M() << ')';
}
#endif

inline std::ostream& operator<< (std::ostream& s, const Darwin::Physics::GenericObject& obj)
{
    return s << obj.p4 << ' ' << obj.scales.size() << ' ' << obj.weights.size();
}
