#ifdef __CLING__

#pragma link C++ class Darwin::Tools::ChainSlice+;
#pragma link C++ class Darwin::Tools::SlicedFriendElement+;
#pragma link C++ class TNotifyLink<Darwin::Tools::SlicedFriendElement>+;

#pragma link C++ class Darwin::Physics::Weight +;

#pragma link C++ class Darwin::Physics::AbstractEvent +;
#pragma link C++ class Darwin::Physics::GenEvent +;
#pragma link C++ class Darwin::Physics::RecEvent +;

#pragma link C++ class Darwin::Physics::AbstractGenericObject +;
#pragma link C++ class Darwin::Physics::GenericObject +;
#pragma link C++ class Darwin::Physics::GenJet +;
#pragma link C++ class Darwin::Physics::RecJet +;
#pragma link C++ class Darwin::Physics::GenMuon +;
#pragma link C++ class Darwin::Physics::RecMuon +;
#pragma link C++ class Darwin::Physics::GenPhoton +;
#pragma link C++ class Darwin::Physics::RecPhoton +;
#pragma link C++ class std::vector<Darwin::Physics::FourVector> +;
#pragma link C++ class std::vector<Darwin::Physics::GenJet> +;
#pragma link C++ class std::vector<Darwin::Physics::RecJet> +;
#pragma link C++ class std::vector<Darwin::Physics::GenMuon> +;
#pragma link C++ class std::vector<Darwin::Physics::RecMuon> +;
#pragma link C++ class std::vector<Darwin::Physics::GenPhoton> +;
#pragma link C++ class std::vector<Darwin::Physics::RecPhoton> +;

#pragma link C++ class Darwin::Physics::Di<Darwin::Physics::GenJet, Darwin::Physics::GenJet> +;
#pragma link C++ class Darwin::Physics::Di<Darwin::Physics::GenMuon, Darwin::Physics::GenMuon> +;
#pragma link C++ class Darwin::Physics::Di<Darwin::Physics::Di<Darwin::Physics::GenMuon, Darwin::Physics::GenMuon>, Darwin::Physics::GenJet> +;
#pragma link C++ class Darwin::Physics::Di<Darwin::Physics::RecJet, Darwin::Physics::GenJet> +;
#pragma link C++ class Darwin::Physics::Di<Darwin::Physics::RecMuon, Darwin::Physics::GenMuon> +;
#pragma link C++ class Darwin::Physics::Di<Darwin::Physics::Di<Darwin::Physics::RecMuon, Darwin::Physics::GenMuon>, Darwin::Physics::GenJet> +;

#endif
