#include <filesystem>
#include <fstream>
#include <memory>
#include <stdexcept>

#include "darwin.h"

#include <TTree.h>

using namespace std;

namespace fs = filesystem;
namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;

namespace Darwin::Tools {

////////////////////////////////////////////////////////////////////////////////
/// Writes a minimal MetaInfo to standard output.
int initMetaInfo (const pt::ptree& config //!< config, with `flags` and `corrections` at least
                  )
{
    auto formatStr = config.get<string>("format");
    MetaInfo::Format format;
    if (formatStr == "info")
        format = MetaInfo::INFO;
    else if (formatStr == "json")
        format = MetaInfo::JSON;
    else if (formatStr == "xml")
        format = MetaInfo::XML;
    else
        BOOST_THROW_EXCEPTION( std::invalid_argument(
            "Unknown MetaInfo format \"" + formatStr + "\"") );

    MetaInfo(MetaInfo::IKnowWhatIAmDoing{}).Write(cout, format);

    return EXIT_SUCCESS;
}

} // namespace Darwin::Tools

namespace DT = Darwin::Tools;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;

        DT::Options options("Generates an empty metainfo and prints it to standard output.");
        options.arg<string>("format", "format", "Output format, info|json|xml");
        return DT::initMetaInfo(options(argc, argv));
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
}
#endif
