#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <iomanip>
#include <filesystem>
#include <memory>

#include "darwin.h"

#include <TFile.h>
#include <TTree.h>

using namespace std;

namespace fs = filesystem;
namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;

namespace Darwin::Tools {

////////////////////////////////////////////////////////////////////////////////
/// Print the content of a 1D histogram in columns.
///
/// Convention: index lowEdge upEdge content error
void printHist (vector<fs::path> inputs, //!< input ROOT files
                const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
                const int steering) //!< bitfield from `Options::steering()`
{
    Flow flow(steering, inputs);
    auto h = flow.GetInputHist(config.get<string>("hist"));
    if (h->GetDimension() > 1)
        BOOST_THROW_EXCEPTION( DE::BadInput("The desired histogram is not 1D",
                                            TFile(inputs.front().c_str()) ));
    int N = h->GetNbinsX();
    for (int i = 0; i <= N+1; ++i) {
        double low = h->GetBinLowEdge(i),
               up  = h->GetBinLowEdge(i+1),
               content = h->GetBinContent(i),
               error = h->GetBinError(i);
        if (error == 0 && content == 0) continue;

        // index
        cout << setw(10);
        if (i == 0) cout << "underflow";
        else if (i == N+1) cout << "overflow";
        else cout << i;

        // low edge
        cout << setw(15);
        if (i == 0) cout << "-inf";
        else cout << low;
        cout << setw(10);

        // upper edge
        if (i > N) cout << "+inf";
        else cout << up;

        // bin content and error
        cout << setw(15) << content
             << setw(15) << error
             << '\n';
    }
    cout << flush;
}

} // end of Darwin::Tools namespace

namespace DT = Darwin::Tools;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;

        DT::Options options("Print the non-empty bins of a histogram");
        options.inputs("inputs", &inputs, "input ROOT files or directory")
               .arg<string>("hist", "hist", "path to histogram to print");
        options(argc, argv);
        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DT::printHist(inputs, config, steering);
        return EXIT_SUCCESS;
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
}
#endif
