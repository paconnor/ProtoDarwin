#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <sstream>
#include <filesystem>
#include <memory>

#include "darwin.h"

#include <TFile.h>
#include <TTree.h>
#include <TString.h>
#include <TKey.h>

using namespace std;

namespace fs = filesystem;

namespace DE = Darwin::Exceptions;

namespace Darwin::Tools {

////////////////////////////////////////////////////////////////////////////////
/// Print content of the first `TTree::UserInfo()` found in the root file.
int getMetaInfo (vector<fs::path> inputs, //!< input ROOT files
                 const fs::path& output //!< output config file (INFO, JSON, XML)
                )
{
    inputs = GetROOTfiles(inputs);

    const size_t N = inputs.size();
    hash<UserInfo> Hash;
    vector<size_t> hashes(N,0);

    const string location = GetFirstTreeLocation(inputs.front());

    // looping over files
    for (size_t i = 0; i < N; ++i) {

        // opening file
        unique_ptr<TFile> fIn(TFile::Open(inputs[i].c_str(), "READ"));
        if (fIn->IsZombie())
            BOOST_THROW_EXCEPTION(DE::BadInput("Can't open the file.", fIn));

        // fetch tree
        auto t = unique_ptr<TTree>(dynamic_cast<TTree*>(fIn->Get<TTree>(location.c_str())));
        if (!t)
            BOOST_THROW_EXCEPTION(DE::BadInput("The tree can't be found in this file.", fIn));

        // open metainfo
        MetaInfo mi(t, false);
        hashes[i] = Hash(mi);

        // print (only the first time---the next rounds, we only check the hash)
        bool found = false;
        if (found) continue;
        mi.Write(output);
        found = true;
    }

    // check the hash
    bool identical = true;
    stringstream ss;
    for (size_t i = 0; i < N; ++i) {
        ss << inputs[i] << '\t' << hex << hashes[i] << '\n';
        if (i == 0) continue;
        identical &= hashes[i-1] == hashes[i];
    }

    if (identical) return EXIT_SUCCESS;
    cerr << red << ss.str() << def;
    return EXIT_FAILURE;
}

}

namespace DT = Darwin::Tools;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Print content of the first `TTree::UserInfo()` found "
                            "in any directory of the first input ROOT file and "
                            "compares it with the content of all other ROOT files "
                            "(returns failure if differences are found).");
        options.inputs("inputs", &inputs, "input ROOT files or directory")
               .output("output", &output, "output file", {".info", ".json", ".xml"});
        options(argc, argv);

        return DT::getMetaInfo(inputs, output);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
}
#endif
