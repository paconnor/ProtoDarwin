#include <cstdlib>
#include <iostream>
#include <filesystem>
#include <memory>
#include <set>
#include <string>

#include "darwin.h"

#include <TFile.h>
#include <TChain.h>
#include <TChainElement.h>
#include <TFriendElement.h>

using namespace std;

namespace fs = filesystem;

namespace DE = Darwin::Exceptions;

namespace Darwin::Tools {

void CollectFriends (const TTree * tree, set<string>& printed,
                     const int steering, string indent = "");

////////////////////////////////////////////////////////////////////////////////
/// Retrieve and prints the elements in the chain and their friends recursively.
void CollectChainFriends
    (const TChain * chain, //!< chain to collect friends from
     set<string>& printed, //!< files already printed
     const int steering, //!< bitfield from `Options::steering()`
     string indent = "") //!< spaces to show the distance from the original tree
{
    for (const auto &obj : *chain->GetListOfFiles()) {
        auto element = dynamic_cast<TChainElement*>(obj);

        // TChain::Add format: filename.root?#tree
        auto full_name = element->GetTitle() + string("?#") + element->GetName();

        // [chain] filename.root?#tree
        cout << indent << "[chain] " << full_name;

        // try to open the file and fetch the tree to check for more friends
        // print extra info if the tree isn't available
        TTree *tree = nullptr;
        unique_ptr<TFile> file(TFile::Open(element->GetTitle()));
        if (file && !file->IsZombie()) {
            tree = file->Get<TTree>(element->GetName());
            if (!tree)
                BOOST_THROW_EXCEPTION(Exceptions::BadInput(
                    Form("Tree %s not found", element->GetName()), file));
        } else
            BOOST_THROW_EXCEPTION(Exceptions::BadInput("File not found", file));
        cout << endl;

        // recurse only if we haven't seen this file yet
        if (printed.count(full_name) == 0) {
            printed.insert(full_name);
            CollectFriends(tree, printed, steering, indent + " ");
        } else
            cout << indent << " [see above]" << endl;
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Retrieve and prints the name and the friends.
///
/// When called on a long chain of friends, the output can include a very large
/// number of files. To avoid printing too many lines, each file is printed only
/// once. The \c printed argument keeps track of files already printed.
void CollectFriends
    (const TTree * tree, //!< tree (or chain) to collect friends from
     set<string>& printed, //!< files already printed
     const int steering, //!< bitflied from `Options::steering()`
     string indent) //!< spaces to show the distance from the original tree
{
    // If `tree` is actually a chain, we go through the files in the chain.
    // There are two reasons to this:
    // - A chain depends on each file in the chain, and we want to display this
    //   information
    // - Each tree in the chain can have its own set of friends independently
    //   from the chain's friends. We also want to print those.
    if (const auto chain = dynamic_cast<const TChain*>(tree); chain)
        CollectChainFriends(chain, printed, steering, indent);

    auto friends = tree->GetListOfFriends();

    // if the tree had no friend, the list does not exist
    if (friends == nullptr) return;

    if (steering & verbose) friends->Print();

    // a priori we only expect one friend, but nothing speaks against more
    for (TObject * obj: *friends) {
        auto buddy = dynamic_cast<TFriendElement*>(obj);

        cout << indent << "[friend] ";
        if (auto sliced = dynamic_cast<SlicedFriendElement*>(obj); sliced)
            // [friend] alias[50:100]
            cout << buddy->GetName()
                 << "[" << sliced->GetBegin() << ":" << sliced->GetEnd() << "]";
        else
            // [friend] filename.root alias
            cout << buddy->GetTitle() << " " << buddy->GetName();

        cout << endl;

        // recurse
        CollectFriends(buddy->GetTree(), printed, steering, indent + " ");
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Prints the recursive list of friends to the input n-tuple.
void printFriends (vector<fs::path> inputs, //!< input ROOT files
                   const int steering) //!< bitfield from `Options::steering()`
{
    inputs = GetROOTfiles(inputs);
    string location = GetFirstTreeLocation(inputs.front());

    Flow flow(steering, inputs);
    auto chain = flow.GetInputTree({1, 0}, location);

    set<string> printed;
    CollectChainFriends(chain, printed, steering);
}

} // end of Darwin::Tools namespace

namespace DT = Darwin::Tools;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;

        DT::Options options("Print the list of friends of the first `TTree`s "
                            "found in any directory of the input ROOT files.");
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory");
        options(argc, argv);
        const int steering = options.steering();

        DT::printFriends(inputs, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
