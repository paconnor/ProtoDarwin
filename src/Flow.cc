#include "Flow.h"
#include "FileUtils.h"

using namespace std;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;

using namespace Darwin::Tools;

Flow::Flow (int steering, const std::vector<std::filesystem::path>& short_inputs)
    : steering(steering), inputs(GetROOTfiles(short_inputs))
{
    if (short_inputs.size() > 0 && inputs.size() == 0)
        BOOST_THROW_EXCEPTION(fs::filesystem_error("No input was found!",
                                make_error_code(errc::no_such_file_or_directory)));
}

Flow::~Flow ()
{
    if (fOut) {
        if (tOut) {
            if (steering & verbose)
                cout << "Flow: saving output tree" << endl;

            fOut->cd();
            tOut->Write();
        }
        else if (steering & verbose)
            cout << "Flow: no output tree to save" << endl;
    }
    else if (tOut && (steering & verbose))
        cout << "Flow: no file to save the output tree" << endl;

    if (tIn) tIn->ResetBranchAddresses();
    if (tOut) tOut->ResetBranchAddresses();
}

ChainSlice * Flow::GetInputTree (const Slice slice, const std::string& name)
{
    if (tIn) {
        if (steering & verbose)
            cout << "Flow: returning the existing input tree "
                    "(arguments are ignored)" << endl;
        return tIn.get();
    }

    if (inputs.size() == 0)
        BOOST_THROW_EXCEPTION( runtime_error("Empty input list") );

    if (slice.first == 0 || slice.first <= slice.second)
        BOOST_THROW_EXCEPTION(invalid_argument("The number of slices " + to_string(slice.first)
              + " must be larger than the index of the current slice " + to_string(slice.second)));

    if (inputs.empty())
        BOOST_THROW_EXCEPTION( invalid_argument("Empty list of input files") );

    tIn = make_unique<ChainSlice>(name.c_str());
    for (const auto& input: inputs) {
        int code = tIn->Add(input.c_str(), 0);
        // "If nentries<=0 and wildcarding is not used,
        // returns 1 if the file exists and contains the correct tree
        //     and 0 otherwise" (ROOT Doxygen)
        if (code == 1) continue; // i.e. the tree was found successfully
        tIn.reset();
        auto fIn = make_unique<TFile>(input.c_str(), "READ");
        BOOST_THROW_EXCEPTION(DE::BadInput("The tree cannot be "
                        "found in (one of) the input file(s).", fIn));
    }
    if (tIn->GetEntries() == 0) {
        tIn.reset();
        BOOST_THROW_EXCEPTION(invalid_argument("Empty trees in input!"));
    }

    // Restrict the tIn to the entries for the slice
    const auto entries = tIn->GetEntries();
    tIn->SetBegin(entries * ((slice.second + 0.) / slice.first));
    tIn->SetEnd(entries * ((slice.second + 1.) / slice.first));

    return tIn.get();
}

TTree * Flow::GetOutputTree (std::shared_ptr<TFile> fOut,
                             const std::source_location location)
{
    if (tOut) {
        if (steering & verbose)
            cout << "Flow: returning the existing output tree" << endl;

        return tOut.get();
    }

    if (fOut)
        SetOutputFile(fOut);

    const char * title = fs::path(location.file_name()).stem().c_str();

    if (!tIn) {
        if (steering & verbose)
            cout << "Flow: creating a new output tree" << endl;

        tOut = make_unique<TTree>("events", title);
    }
    else if (steering & Friend) {
        /// \note We use the title to store the current command.
        /// When the present tree is used as a friend, this title will supsersede its name.
        /// Exceptions:
        /// - no title was found,
        /// - or the present name is the same as the last title (i.e. running twice the same command).
        if (steering & verbose)
            cout << "Flow: creating a friend tree" << endl;

        tOut = make_unique<TTree>("events", title);
        tIn->GetEntry(0); // necessary to load a(ny) TTree

        // find the name of the previous command
        string lastFunc = tIn->GetTree()->GetTitle();
        // -> this is supposed to store the name of the last command
        if (lastFunc == "") {
            cerr << orange << "Unable to identify the command used to "
                              "obtain the input from the title.\n" << def;
            lastFunc = "previous";
        }
        else if (lastFunc == title) {
            cerr << orange << "Running twice the same command in a row. "
                              "Adding `previous2` ahead of the title.\n" << def;
            lastFunc = "previous2"s + title;
        }

        // add friend
        SlicedFriendElement::AddTo(tOut.get(), tIn.get(), lastFunc.c_str());

        // copy metainfo
        TList * userinfo = tIn->GetTree()->GetUserInfo();
        tOut->GetUserInfo()->AddAll(userinfo);
        userinfo->Clear(); // necessary to prevent the TTree destructor from deleting
                           // all elements in the list (avoid double deletion)
    }
    else {
        if (steering & verbose)
            cout << "Flow: creating a clone tree" << endl;

        tOut = unique_ptr<TTree>(tIn->CloneTree(0));
        tOut->SetTitle(title);
    }
    if (fOut)
        tOut->SetDirectory(fOut.get());
    return tOut.get();
}

TTree * Flow::GetOutputTree (const std::filesystem::path& fOutName,
                             const std::source_location location)
{
    shared_ptr<TFile> fOut = Darwin::Tools::GetOutputFile(fOutName, location);
    return GetOutputTree(fOut, location);
}

std::pair<TFile *, TTree *> Flow::GetOutput (const std::filesystem::path& fOutName,
                                             const std::source_location location)
{
    shared_ptr<TFile> fOut = Darwin::Tools::GetOutputFile(fOutName, location);
    return {fOut.get(), GetOutputTree(fOut, location)};
}
