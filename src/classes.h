#pragma once

#include <Di.h>
#include <Event.h>
#include <FriendUtils.h>
#include <Jet.h>
#include <Lepton.h>
#include <Photon.h>

Darwin::Physics::GenEvent Darwin__genevent;
Darwin::Physics::RecEvent Darwin__recevent;

Darwin::Physics::GenJet Darwin__genjet;
Darwin::Physics::RecJet Darwin__recjet;
Darwin::Physics::GenMuon Darwin__genmuon;
Darwin::Physics::RecMuon Darwin__recmuon;
Darwin::Physics::GenPhoton Darwin__genlepton;
Darwin::Physics::RecPhoton Darwin__reclepton;

std::vector<Darwin::Physics::FourVector> Darwin__p4s;
std::vector<Darwin::Physics::GenJet> Darwin__genjets;
std::vector<Darwin::Physics::RecJet> Darwin__recjets;
std::vector<Darwin::Physics::GenMuon> Darwin__genmuons;
std::vector<Darwin::Physics::RecMuon> Darwin__recmuons;
std::vector<Darwin::Physics::GenPhoton> Darwin__genphotons;
std::vector<Darwin::Physics::RecPhoton> Darwin__recphotons;

Darwin::Physics::Di<Darwin::Physics::GenJet,Darwin::Physics::GenJet> Darwin__gendijet;
Darwin::Physics::Di<Darwin::Physics::GenMuon,Darwin::Physics::GenMuon> Darwin__gendimuon;
Darwin::Physics::Di<Darwin::Physics::Di<Darwin::Physics::GenMuon,Darwin::Physics::GenMuon>,Darwin::Physics::GenJet> Darwin__genzjet;
Darwin::Physics::Di<Darwin::Physics::RecJet,Darwin::Physics::RecJet> Darwin__recdijet;
Darwin::Physics::Di<Darwin::Physics::RecMuon,Darwin::Physics::RecMuon> Darwin__recdimuon;
Darwin::Physics::Di<Darwin::Physics::Di<Darwin::Physics::RecMuon,Darwin::Physics::RecMuon>,Darwin::Physics::RecJet> Darwin__reczjet;
