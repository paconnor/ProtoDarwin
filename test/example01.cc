#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <filesystem>
#include <string>
#include <optional>
#include <memory>

#include <darwin.h>
#include <Event.h>
#include <Jet.h>

#include <boost/property_tree/ptree.hpp>      

#include <TFile.h>
#include <TTree.h>
#include <TRandom3.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Pure Gaussian smearing with realistic values estimated from jet from CMS at
/// 13 TeV.
class Smear {

    float N2, //!< noise parameter (squared)
          S2, //!< stochastic parameter (squared)
          C2; //!< constant parameter (squared)
    mutable TRandom3 r; //!< random number generator

public:
    
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor. Takes the parameters of the resolution function and a seed.
    Smear (float N, //!< noise parameter
           float S, //!< stochastic parameter
           float C, //!< constant parameter
           unsigned seed //!< seed for Gaussian number generator
           ) : N2(pow(N,2)), S2(pow(S,2)), C2(pow(C,2)), r(seed)
    { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Smearing:
    /// \f[
    ///    p_\mathrm{T}^\text{rec} = p_\mathrm{T}^\text{gen} \left( 1 + \delta \right)
    /// \f]
    /// with Gaussian reponse
    /// \f[
    ///    \delta \sim \mathcal{N}\left(0,\sigma(p_\mathrm{T}^\text{gen})^2\right)
    /// \f]
    /// where \f$\sigma$ is given by a NSC function:
    /// \f[
    ///    \frac{\sigma(p_\mathrm{T})}{p_\mathrm{T}} = \sqrt{ \frac{N^2}{p_\mathrm{T}^2} + \frac{S^2}{p_\mathrm{T}} + C^2 }
    /// \f]
    float operator() (float genpt //!< gen-level transverse momentum
                     ) const
    {
        float resolution = sqrt( N2 / pow( genpt, 2) + S2 / genpt + C2 );
        float response = r.Gaus(0, resolution);
        float recpt = genpt * (1+response);
        return recpt;
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Toy example to generate a n-tuple and toy jets.
///
/// The template argument is only for the test of exceptions in unit tests.
template<bool DARWIN_TEST_EXCEPTIONS = false>
void example01 (const fs::path& output, //!< output ROOT file
                const pt::ptree& config, //!< config, with `flags` and `corrections` at least
                const int steering, //!< parameters obtained from explicit options 
                const DT::Slice slice = {1,0} //!< number and index of slice
                )
{
    cout << __func__ << " start" << endl;

    bool isMC = config.get<bool>("flags.isMC");

    DT::Flow flow(steering);
    auto tOut = flow.GetOutputTree(output);

    auto recEvent =        flow.GetBranchWriteOnly<RecEvent>("recEvent");
    auto genEvent = isMC ? flow.GetBranchWriteOnly<GenEvent>("genEvent") : nullptr;

    auto recJets =        flow.GetBranchWriteOnly<vector<RecJet>>("recJets");
    auto genJets = isMC ? flow.GetBranchWriteOnly<vector<GenJet>>("genJets") : nullptr;

    const int maxN = 10;
    recJets->reserve(maxN);
    if (isMC)
        genJets->reserve(maxN);

    DT::MetaInfo metainfo(tOut, config);
    if constexpr (DARWIN_TEST_EXCEPTIONS)
        BOOST_THROW_EXCEPTION(DE::BadInput("this is just a test", metainfo));

    const float minpt = 10, maxpt = 1000;
    Smear smear(4.4, 1.4, 0.03, metainfo.Seed<8237401>(slice));

    TRandom3 r(metainfo.Seed<34780275>(slice));
    for (DT::Looper looper(100ll/slice.first); looper(); ++looper) {
        [[maybe_unused]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        if (isMC)
            genJets->clear();
        recJets->clear();

        recEvent->fill = 1234;
        recEvent->runNo = 424242;
        recEvent->lumi = 123456789;
        recEvent->evtNo = *looper;

        if (isMC)
            genEvent->hard_scale = 666;

        int N = r.Uniform()*maxN;
        for (int iJet = 0; iJet < N; ++iJet) {
            float genpt = r.Uniform(minpt, maxpt),
                  recpt = clamp(smear(genpt), minpt, maxpt);
            cout << setw(10) << iJet << setw(10) << genpt << setw(10) << recpt << endl;
            GenJet genJet;
            RecJet recJet;
            genJet.p4.SetPt(genpt);
            recJet.p4.SetPt(recpt);
            if (isMC)
                genJets->push_back(genJet);
            recJets->push_back(recJet);
        }

        if (steering & DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << " end" << endl;
}

} // end of namespace

namespace DP = Darwin::Physics;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        fs::path output;

        DT::Options options("Toy example to generate a n-tuple.", DT::config | DT::split | DT::fill);
        options.output("output"    , &output, "output ROOT file")
               .arg<bool>("isMC"  , "flags.isMC"  , "flag"                                         )
               .arg<int >("R"     , "flags.R"     , "R parameter in jet clustering algorithm (x10)")
               .arg<int >("year"  , "flags.year"  , "year (20xx)"                                  )
               .args     ("labels", "flags.labels", "labels (multitoken, optional)"                );
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DP::example01(output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
