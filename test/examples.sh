#!/usr/bin/env zsh
set -e
set -x

source ../setup

rm -rf loop/
mkdir loop
cp example0? example.info loop/
cd loop/

run () {
    parallel ./example01 Ntuple01 -c $1 -f -j 2
    ./example04 "Ntuple01/*.root" Hists04_initial.root
    printHist Hists04_initial.root hgen > Hists04_gen_initial.txt

    parallel ./example02 "Ntuple01/*.root" Ntuple02_noFriends -c $1 -f -j 3
    [ $(printFriends Ntuple02_noFriends/0.root | wc -l) = 1 ]  # There should be no friends here
    parallel ./example03 "Ntuple02_noFriends/*.root" Ntuple03_noFriends -f -s -j 4
    try ./example04 "Ntuple03_noFriends/*.root" Hists04_noFriends/ -s
    printHist Hists04_noFriends hrecnominal > Hists04_rec_noFriends.txt
    printHist Hists04_noFriends hgen > Hists04_gen_noFriends.txt
    diff Hists04_gen_initial.txt Hists04_gen_noFriends.txt

    parallel ./example02 "Ntuple01/*.root" Ntuple02_friends -c $1 -F -j 3
    printFriends Ntuple02_friends
    [ $(printFriends Ntuple02_friends/0.root | wc -l) = 4 ]  # There should be friends here
    parallel ./example03 "Ntuple02_friends/*.root" Ntuple03_friends -F -s -j 4
    try ./example04 "Ntuple03_friends/*.root" Hists04_friends/ -v -s
    printHist Hists04_friends hrecnominal > Hists04_rec_friends.txt
    printHist Hists04_friends hgen > Hists04_gen_friends.txt
    diff Hists04_gen_initial.txt Hists04_gen_friends.txt

    getMetaInfo Hists04_friends/0.root $2
    diff Hists04_rec_friends.txt Hists04_rec_noFriends.txt
    diff Hists04_gen_friends.txt Hists04_gen_noFriends.txt
}

./example01 -h -t -e
[[ "$(./example01 -i)" == "" ]]
[[ "$(./example02 -i)" == ".root .xml" ]]
[[ "$(./example03 -i)" == ".root .xml" ]]
[[ "$(./example04 -i)" == ".root .xml" ]]
[[ "$(./example01 -o)" == ".root .xml" ]]
[[ "$(./example02 -o)" == ".root .xml" ]]
[[ "$(./example03 -o)" == ".root .xml" ]]
[[ "$(./example04 -o)" == ".root .xml" ]]
run example.info run1.info
cat run1.info
rm -rf Ntuple0*/ Hists04_info/
mv Hists04_noFriends/ Hists04_info
run run1.info run2.json
python -mjson.tool run2.json > /dev/stdout
cat run2.json
rm -rf Ntuple0*/ Hists04_json/
mv Hists04_noFriends/ Hists04_json
run run2.json run3.xml
cat run3.xml
xmllint --noout run3.xml
rm -rf Ntuple0*/ Hists04_xml/
mv Hists04_noFriends/ Hists04_xml
run run3.xml run4.info
diff run1.info run4.info
getMetaInfo "Hists04_*" /dev/stdout || true
getMetaInfo Ntuple02_noFriends/ /dev/stdout
