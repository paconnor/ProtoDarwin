#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE testUserInfo
#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>
#include <boost/property_tree/info_parser.hpp>

#include "test.h"
#include "UserInfo.h"

using namespace std;
namespace fs = std::filesystem;
namespace pt = boost::property_tree;

namespace DT = Darwin::Tools;

BOOST_AUTO_TEST_SUITE( user_info )

    BOOST_AUTO_TEST_CASE( arithmetic )
    {
        DT::UserInfo userinfo;

        bool myBool = true;
        int myInt = 2016;
        float myFloat = 3.1415;
        double myDouble = 3.141592404915836;
        BOOST_REQUIRE_NO_THROW( userinfo.Set<bool  >("closure", "arithmetic", "myBool"  , myBool  ) );
        BOOST_REQUIRE_NO_THROW( userinfo.Set<int   >("closure", "arithmetic", "myInt"   , myInt   ) );
        BOOST_REQUIRE_NO_THROW( userinfo.Set<float >("closure", "arithmetic", "myFloat" , myFloat ) );
        BOOST_REQUIRE_NO_THROW( userinfo.Set<double>("closure", "arithmetic", "myDouble", myDouble) );
        BOOST_TEST( myBool   == userinfo.Get<bool  >("closure", "arithmetic", "myBool"  ) );
        BOOST_TEST( myInt    == userinfo.Get<int   >("closure", "arithmetic", "myInt"   ) );
        BOOST_TEST( myFloat  == userinfo.Get<float >("closure", "arithmetic", "myFloat" ) );
        BOOST_TEST( myDouble == userinfo.Get<double>("closure", "arithmetic", "myDouble") );
        BOOST_REQUIRE_THROW( userinfo.Get<double>("closure", "arithmetic", "myBool"          ), boost::wrapexcept<std::invalid_argument> );
        BOOST_REQUIRE_THROW( userinfo.Set<double>("closure", "arithmetic", "myBool", myDouble), boost::wrapexcept<std::invalid_argument> );
        BOOST_TEST( userinfo.Find("closure", "arithmetic", "myBool") );
        BOOST_TEST( userinfo.Find("closure", "arithmetic", "sine") == false );
        BOOST_TEST( userinfo.Find("closure", "trigonometry", "sine") == false );
    }

    BOOST_AUTO_TEST_CASE( literal )
    {
        DT::UserInfo userinfo;

        const char * myConstChar = "hello, world";
        string myString = "Darwin";
        fs::path myPath = "example.info";
        BOOST_REQUIRE_NO_THROW( userinfo.Set<const char *>("closure", "literals", "myConstChar", myConstChar) );
        BOOST_REQUIRE_NO_THROW( userinfo.Set<string      >("closure", "literals", "myString"   , myString   ) );
        BOOST_REQUIRE_NO_THROW( userinfo.Set<fs::path    >("closure", "literals", "myPath"     , myPath     ) );
        BOOST_TEST( strcmp(myConstChar, userinfo.Get<const char *>("closure", "literals", "myConstChar")) == 0 );
        BOOST_TEST(        myString  == userinfo.Get<string      >("closure", "literals", "myString"   )       );
        BOOST_TEST(        myPath    == userinfo.Get<fs::path    >("closure", "literals", "myPath"     )       );
        BOOST_REQUIRE_THROW( userinfo.Get<const char *>("this", "does", "not", "exist"), boost::wrapexcept<std::invalid_argument> );
    }

    BOOST_AUTO_TEST_CASE( writing )
    {
        DT::UserInfo userinfo;

        BOOST_REQUIRE_NO_THROW( userinfo.Set<const char *>("corrections", "JES", "nominal") );
        BOOST_REQUIRE_NO_THROW( userinfo.Set<const char *>("corrections", "JES", "JESup"  ) );
        BOOST_REQUIRE_NO_THROW( userinfo.Set<const char *>("corrections", "JES", "JESdn"  ) );

        BOOST_REQUIRE_NO_THROW( userinfo.Write("test.info") );
        BOOST_REQUIRE_NO_THROW( userinfo.Write("test.json") );
        BOOST_REQUIRE_NO_THROW( userinfo.Write("test.xml") );
        fs::remove("test.info");
        fs::remove("test.json");
        fs::remove("test.xml");
    }

    BOOST_AUTO_TEST_CASE( loading )
    {
        DT::UserInfo userinfo;

        BOOST_REQUIRE_NO_THROW( userinfo.Set<const char *>("corrections", "JES", "nominal") );
        BOOST_REQUIRE_NO_THROW( userinfo.Set<const char *>("corrections", "JES", "JESup"  ) );
        BOOST_REQUIRE_NO_THROW( userinfo.Set<const char *>("corrections", "JES", "JESdn"  ) );
        BOOST_REQUIRE_NO_THROW( userinfo.Set<int>  ("answer", 42     ) );
        BOOST_REQUIRE_NO_THROW( userinfo.Set<float>("pi"    , 3.1415f) );

        stringstream ss;
        auto pt = userinfo.Write(ss, DT::UserInfo::INFO);

        // Check round trip. We deserialize the ptree and serialize it again
        DT::UserInfo reconstructed(pt);

        stringstream ss2;
        BOOST_REQUIRE_NO_THROW( reconstructed.Write(ss2, DT::UserInfo::INFO) );
        BOOST_REQUIRE( ss.str() == ss2.str() );
    }

    BOOST_AUTO_TEST_CASE( hashing )
    {
        DT::UserInfo userinfo1, userinfo2;
        userinfo1.Set<int>("test", 1);
        userinfo2.Set<int>("test", 1);

        hash<DT::UserInfo> Hash;
        BOOST_TEST( Hash(userinfo1) == Hash(userinfo2) );
        userinfo2.Set<int>("test", 2);
        BOOST_TEST( Hash(userinfo1) != Hash(userinfo2) );
    }

BOOST_AUTO_TEST_SUITE_END()

#endif
