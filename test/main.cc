#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE test
#include <boost/test/included/unit_test.hpp>
#include <boost/exception/all.hpp>

#include <boost/property_tree/ptree.hpp>      
#include <boost/property_tree/info_parser.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/xml_parser.hpp>

#include "test.h"
#include "exceptions.h"
#include "Options.h"
#include "UserInfo.h"
#include "MetaInfo.h"

#ifndef DARWIN
#error "DARWIN (containing the absolute path to the root of the repo) must be given to gcc with option `-D`"
#endif
#define DARWIN_GIT_REPO DARWIN

#include "example01.cc"
#include "example02.cc"
#include "example03.cc"
#include "example04.cc"
#include "../bin/getMetaInfo.cc"
#include "../bin/printMetaInfo.cc"
#include "../bin/printEntries.cc"
#include "../bin/printBranches.cc"
#include "../bin/forceMetaInfo.cc"

using namespace std;

namespace DT = Darwin::Tools;
using DT::tokenize;

using boost::wrapexcept;

namespace Darwin::Physics {

template<bool DARWIN_TEST_EXCEPTIONS = false>
void example02 (const fs::path& input, const fs::path& output,
                const pt::ptree& config, const int steering,
                const DT::Slice slice = {1,0} )
{
    example02<DARWIN_TEST_EXCEPTIONS>(vector<fs::path>{input}, output, config, steering, slice);
}

template<bool DARWIN_TEST_EXCEPTIONS = false>
void example03 (const fs::path& input, const fs::path& output,
                const pt::ptree& config, const int steering,
                const DT::Slice slice = {1,0} )
{
    example03<DARWIN_TEST_EXCEPTIONS>(vector<fs::path>{input}, output, config, steering, slice);
}

void example04 (const fs::path& input, const fs::path& output,
                const pt::ptree& config, const int steering,
                const DT::Slice slice = {1,0})
{
    example04(vector<fs::path>{input}, output, config, steering, slice);
}

}
namespace DP = Darwin::Physics;
namespace DE = Darwin::Exceptions;

BOOST_AUTO_TEST_SUITE( test_hadd_like_fcts )

    BOOST_AUTO_TEST_CASE( output )
    {
        DT::Flow flow(DT::verbose);
        BOOST_REQUIRE_THROW( DT::GetOutputFile("/this/file/does/not/exists.root"), wrapexcept<fs::filesystem_error> );
        BOOST_REQUIRE_NO_THROW( DT::GetOutputFile("this_file_is_allowed.root") );;
    }

    BOOST_AUTO_TEST_CASE( getters_empty_input )
    {
        vector<fs::path> files;
        DT::Flow flow(DT::none, files);
        BOOST_TEST_MESSAGE( "Testing exceptions for empty list of input files" );
        BOOST_REQUIRE_THROW( flow.GetInputHist("h")        , wrapexcept<runtime_error> );
        BOOST_REQUIRE_THROW( flow.GetInputTree({1, 0}, "t"), wrapexcept<runtime_error> );
    }

    BOOST_AUTO_TEST_CASE( getters_broken_input )
    {
        vector<fs::path> files{"/this/dir/does/not/exist"};
        BOOST_REQUIRE_THROW( DT::Flow(DT::verbose, files), wrapexcept<fs::filesystem_error> );
    }

    BOOST_AUTO_TEST_CASE( getters_wrong_objects )
    {
        vector<fs::path> files;
        for (int i = 1; i < 4; ++i) {
            fs::path name = Form("getters_%d.root", i);
            cout << name << endl;
            files.push_back(name);
            unique_ptr<TFile> f(TFile::Open(name.c_str(), "RECREATE"));
            auto h = make_unique<TH1F>("h", "h", 1, 0., 1.);
            auto t = make_unique<TTree>("t", "t");
            h->SetDirectory(f.get());
            t->SetDirectory(f.get());
            h->Write();
            t->Write();
        }

        DT::Flow flow(DT::none, files);
        BOOST_TEST_MESSAGE( "Testing exceptions for inexisting histogram" );
        BOOST_REQUIRE_THROW( flow.GetInputHist(        "thisHistDoesNotExist"), wrapexcept<DE::BadInput> );
        BOOST_TEST_MESSAGE( "Testing exceptions for empty tree" );
        BOOST_REQUIRE_THROW( flow.GetInputTree({1, 0}, "t"                   ), wrapexcept<invalid_argument> );
        BOOST_TEST_MESSAGE( "Testing exceptions for inexisting tree" );
        BOOST_REQUIRE_THROW( flow.GetInputTree({1, 0}, "thisTreeDoesNotExist"), wrapexcept<DE::BadInput> );

        BOOST_REQUIRE_NO_THROW( flow.GetInputHist("h") );
    }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( test_examples )

    BOOST_AUTO_TEST_CASE( ntuple_producer )
    {
        fs::path output;

        DT::Options options("This is just a toy example to generate a n-tuple.", DT::config | DT::fill);
        options.output("output"    , &output, "output ROOT file")
               .arg<bool>  ("isMC" , "flags.isMC" , "flag"                                   )
               .arg<float> ("R"    , "flags.R"    , "R parameter in jet clustering algorithm")
               .arg<int  > ("year" , "flags.year" , "year (20xx)"                            );

        auto const args = tokenize("example01 ntuple01.root -c example.info -f");
        BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );

        const auto& config = options(args.size(), args.data());
        const int steering = options.steering();

        BOOST_REQUIRE_NO_THROW( DP::example01(output, config, steering) );
    }

    BOOST_AUTO_TEST_CASE( ntuple_weight_modifier )
    {
        fs::path input, output;

        DT::Options options("Toy example to modify a n-tuple (filter application).",
                            DT::config | DT::split | DT::fill | DT::Friend);
        options.input ("input" , &input , "input ROOT file" )
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("filters", "corrections.filters", "location of list of event filters to apply");

        for (string mode: {"fill", "Friend"}) {
            string cmd = "example02 ntuple01.root ntuple02_" + mode + ".root -c example.info -v -j 2 -k 1 -" + mode.at(0);
            auto const args = tokenize(cmd.c_str());
            options(args.size(), args.data());
            BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );

            const auto& config = options(args.size(), args.data());
            const auto& slice = options.slice();
            const int steering = options.steering();

            DP::example02(input, output, config, steering, slice);
            BOOST_REQUIRE_NO_THROW( DP::example02(input, output, config, steering, slice) );
        }

        DP::RecEvent * event_f = nullptr,
                     * event_F = nullptr;
        DT::Flow flow_f(DT::verbose             , {"ntuple02_fill.root"}),
                 flow_F(DT::verbose | DT::Friend, {"ntuple02_Friend.root"});

        auto tIn_f = flow_f.GetInputTree(),
             tIn_F = flow_F.GetInputTree();
        BOOST_TEST( tIn_f->GetEntries() == tIn_F->GetEntries() );

        BOOST_TEST_MESSAGE( "Trying to load an inexistant branch" );
        BOOST_TEST(          flow_f.GetBranchReadOnly<DP::RecEvent>("event", DT::facultative) == nullptr );
        BOOST_REQUIRE_THROW( flow_f.GetBranchReadOnly<DP::RecEvent>("event", DT::mandatory  ), wrapexcept<DE::BadInput> );

        BOOST_TEST_MESSAGE( "Loading the analog branch in the friend and no-friend n-tuples." );
        BOOST_REQUIRE_NO_THROW( event_f = flow_f.GetBranchReadOnly<DP::RecEvent>("recEvent") );
        BOOST_REQUIRE_NO_THROW( event_F = flow_F.GetBranchReadOnly<DP::RecEvent>("recEvent") );

        for (long long i = 0; i < tIn_f->GetEntries(); ++i) {
            tIn_f->GetEntry(i);
            tIn_F->GetEntry(i);
            BOOST_TEST( event_f->weights.front() == event_F->weights.front() );
        }
    }

    BOOST_AUTO_TEST_CASE( ntuple_JEC_modifier )
    {
        fs::path input, output;

        DT::Options options("Toy example to modify a n-tuple (JES corrections).",
                            DT::split | DT::fill | DT::syst | DT::Friend);
        options.input ("input" , &input , "input ROOT file" )
               .output("output", &output, "output ROOT file");

        for (string mode: {"fill", "Friend"}) {
            string cmd = "example03 ntuple02_" + mode + ".root ntuple03_" + mode + ".root -j 3 -k 1 -" + mode.at(0) + " -s";
            cout << cmd << endl;
            auto const args = tokenize(cmd.c_str());
            options(args.size(), args.data());
            BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );

            const auto& config = options(args.size(), args.data());
            const auto& slice = options.slice();
            const int steering = options.steering();

            BOOST_REQUIRE_NO_THROW( DP::example03(input, output, config, steering, slice) );
        }

        vector<DP::RecJet> * recs_f = nullptr,
                           * recs_F = nullptr;
        DT::Flow flow_f(DT::verbose             , {"ntuple03_fill.root"}),
                 flow_F(DT::verbose | DT::Friend, {"ntuple03_Friend.root"});
        flow_f.GetInputTree();
        flow_F.GetInputTree();
        BOOST_REQUIRE_NO_THROW( recs_f = flow_f.GetBranchReadOnly<vector<DP::RecJet>>("recJets") );
        BOOST_REQUIRE_NO_THROW( recs_F = flow_F.GetBranchReadOnly<vector<DP::RecJet>>("recJets") );

        string active_branches_f = flow_f.DumpActiveBranches(),
               active_branches_F = flow_F.DumpActiveBranches();
        cout << "Without friends: " << active_branches_f << endl;
        cout << "With friends: " << active_branches_F << endl;
        BOOST_TEST( active_branches_f == active_branches_F );

        auto tIn_f = flow_f.GetInputTree(),
             tIn_F = flow_F.GetInputTree();
        BOOST_TEST( tIn_f->GetEntries() == tIn_F->GetEntries() );

        for (long long i = 0; i < tIn_f->GetEntries(); ++i) {
            tIn_f->GetEntry(i);
            tIn_F->GetEntry(i);
            BOOST_TEST( recs_f->size() == recs_F->size() );
            for (size_t j = 0; j < recs_f->size(); ++j)
                BOOST_TEST( recs_f->at(j).CorrPt() == recs_F->at(j).CorrPt() );
        }

        fs::remove("ntuple02_fill.root");
        fs::remove("ntuple03_fill.root");
    }

    BOOST_AUTO_TEST_CASE( projection )
    {
        fs::path input, output;

        DT::Options options("Toy example to project a n-tuple.", DT::split);
        options.input ("input" , &input , "input ROOT file" )
               .output("output", &output, "output ROOT file");

        BOOST_TEST( fs::exists("ntuple03_Friend.root") );
        auto const args = tokenize("example04 ntuple03_Friend.root hists04.root -v");
        BOOST_REQUIRE_NO_THROW( options(args.size(), args.data()) );

        const auto& config = options(args.size(), args.data());
        const auto& slice = options.slice();
        const int steering = options.steering();

        BOOST_REQUIRE_NO_THROW( DP::example04(input, output, config, steering, slice) );

        fs::remove("ntuple01.root");
        fs::remove("ntuple02_Friend.root");
        fs::remove("ntuple03_Friend.root");
        fs::remove("hists04.root");
    }

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE( whole_cycle )

    BOOST_AUTO_TEST_CASE( all_steps )
    {
        pt::ptree config;
        BOOST_REQUIRE_NO_THROW( pt::read_info("example.info", config) );
        BOOST_REQUIRE_NO_THROW( DT::Options::parse_config(config) );
        const int steering = DT::fill | DT::syst;
        BOOST_REQUIRE_NO_THROW( DP::example01("ntuple01.root", config, steering) );
        BOOST_REQUIRE_NO_THROW( DP::example02("ntuple01.root", "ntuple02.root", config, steering) );
        BOOST_REQUIRE_NO_THROW( DP::example03("ntuple02.root", "ntuple03.root", config, steering) );
        BOOST_REQUIRE_NO_THROW( DP::example04("ntuple03.root", "hists04.root", config, steering) );
    }

    BOOST_AUTO_TEST_CASE( getBool )
    {
        BOOST_TEST( !DT::getBool("this will not work").has_value());
        BOOST_TEST( DT::getBool("yes").has_value());
        BOOST_TEST( DT::getBool("true" ).value() == true );
        BOOST_TEST( DT::getBool("kTRUE").value() == true );
        BOOST_TEST( DT::getBool("off"  ).value() == false );
        BOOST_TEST( DT::getBool("NO"   ).value() == false );
    }

    BOOST_AUTO_TEST_CASE( forceMetaInfo )
    {
        vector<fs::path> input = {"ntuple02.root"};
        DT::getMetaInfo(input, "force02.info");
        DT::forceMetaInfo("force02.info", "ntuple01.root", DT::verbose);
        input = {"ntuple01.root"};
        DT::getMetaInfo(input, "force01.info");
        unique_ptr<TFile> f(TFile::Open("ntuple01.root", "READ"));
        auto events = unique_ptr<TTree>(f->Get<TTree>("events"));
        DT::MetaInfo metainfo(events);
        BOOST_TEST( !metainfo.Find("history") );
        BOOST_TEST( !metainfo.Get<bool>("git", "reproducible") );
    }

    BOOST_AUTO_TEST_CASE( GetFirstTreeLocation )
    {
        BOOST_TEST( DT::GetFirstTreeLocation("ntuple01.root") == "events" );
        BOOST_REQUIRE_NO_THROW( DT::GetFirstTreeLocation("ntuple01.root") );

        TFile::Open("GetFirstTreeLocation.root", "RECREATE")->Close();
        BOOST_REQUIRE_THROW( DT::GetFirstTreeLocation("GetFirstTreeLocation.root"), wrapexcept<DE::BadInput> );
    }

    BOOST_AUTO_TEST_CASE( printEntries )
    {
        auto f = TFile::Open("printEntries.root", "RECREATE");
        f->mkdir("dir1");
        auto d = f->mkdir("dir2"); d->cd();
        auto dd = d->mkdir("subdir"); dd->cd();
        auto t = new TTree("one_tree", "");
        t->SetDirectory(dd);
        t->Write();
        f->Close();
        BOOST_REQUIRE_NO_THROW( DT::printEntries({"printEntries.root"}, DT::verbose) );
        fs::remove("printEntries.root");
    }

    BOOST_AUTO_TEST_CASE( printBranches )
    {
        vector<fs::path> inputs {"ntuple01.root"};
        BOOST_REQUIRE_NO_THROW( DT::printBranches(inputs, DT::verbose) );
    }

    BOOST_AUTO_TEST_CASE( printMetaInfo )
    {
        vector<fs::path> inputs {"ntuple01.root"};
        pt::ptree tree;
        tree.put<string>("path", "preseed");
        BOOST_REQUIRE_NO_THROW( DT::printMetaInfo(inputs, tree) );
    }

    BOOST_AUTO_TEST_CASE( closure )
    {
        {
            unique_ptr<TFile> f(TFile::Open("ntuple02.root"));
            f->ls();
        }

        vector<string> exts {"info", "xml", "json"};
        for (string ext: exts) {
            fs::path file = "out1." + ext;
            vector<fs::path> inputs = {"ntuple02.root"};
            BOOST_REQUIRE_NO_THROW( DT::getMetaInfo(inputs, file) );
        }

        auto getHash = [](const fs::path& ntuple) {
            auto rootfile = unique_ptr<TFile>(TFile::Open(ntuple.c_str(), "READ"));
            auto events = unique_ptr<TTree>(rootfile->Get<TTree>("events"));
            size_t Hash = hash<TTree*>{}(events.get());
            return Hash;
        };

        size_t Hash = getHash("ntuple02.root");

        pt::ptree config;
        auto chain = [&config,&getHash](const fs::path& fOut, bool isMC) {
            fs::remove("ntuple02.root");
            fs::remove("ntuple03.root");
            config.put<bool>("flags.isMC", isMC);
            const int steering = DT::fill | DT::syst;
            BOOST_REQUIRE_NO_THROW( DT::Options::parse_config(config) );
            DP::example01("ntuple01.root", config, steering);
            DP::example02("ntuple01.root", "ntuple02.root", config, steering);
            DP::example03("ntuple02.root", "ntuple03.root", config, steering);
            DP::example04("ntuple03.root", "hists04.root", config, steering);
            vector<fs::path> inputs = {"hists04.root"};
            DT::getMetaInfo(inputs, fOut);
            size_t Hash = getHash("ntuple02.root");
            fs::remove("ntuple01.root");
            fs::rename("hists04.root", Form("hists04_%s_%s.root", &(fOut.extension().c_str()[1]), isMC ? "true" : "false"));
            return Hash;
        };

        pt::read_info("out1.info", config);
        int oldseed = config.get<int>("preseed");
        config.put<int>("preseed", 2*oldseed);
        BOOST_TEST( Hash != chain("out2.info", true ) );
        config.put<int>("preseed", oldseed);
        BOOST_TEST( Hash == chain("out2.info", true ) );
        BOOST_TEST( Hash != chain("out2.info", false) );

        pt::read_json("out1.json", config);
        BOOST_TEST( Hash == chain("out2.json", true ) );
        BOOST_TEST( Hash != chain("out2.json", false) );

        pt::read_xml("out1.xml", config);
        config = config.get_child("userinfo");
        BOOST_TEST( Hash == chain("out2.xml", true ) );
        BOOST_TEST( Hash != chain("out2.xml", false) );

        vector<fs::path> inputs = {"hists04_info_true.root" , "hists04_json_true.root" , "hists04_xml_true.root" };
        BOOST_TEST( DT::getMetaInfo(inputs, "true.info"   ) == EXIT_SUCCESS );
        inputs = {"hists04_info_false.root", "hists04_json_false.root", "hists04_xml_false.root"};
        BOOST_TEST( DT::getMetaInfo(inputs, "false.info"  ) == EXIT_SUCCESS );
        inputs = {"hists04_info_true.root" , "hists04_info_false.root"};
        BOOST_TEST( DT::getMetaInfo(inputs, "failure.info") == EXIT_FAILURE );
        
        for (string name: {"true", "false", "failure"})
            fs::remove(name + ".info");

        for (string ext: exts) {
            fs::remove("out1." + ext);
            fs::remove("out2." + ext);
            for (string b: {"true", "false"})
                fs::remove("hists04_" + ext + "_" + b + ".root");
        }
    }

    BOOST_AUTO_TEST_CASE( exceptions )
    {
        // testing raw exception object
        auto f = unique_ptr<TFile>(TFile::Open("ntuple02.root", "READ"));
        auto t = unique_ptr<TTree>(f->Get<TTree>("events"));
        DT::MetaInfo metainfo(t);
        cout << DE::BadInput("Here is a bad input", metainfo).what() << endl;
        t->GetEntry(5);
        cout << DE::AnomalousEvent("Here is an anomally (just for the example)", t).what() << endl;

        // testing in real context
        pt::ptree config;
        BOOST_REQUIRE_NO_THROW( pt::read_info(DARWIN "/test/example.info", config) );
        BOOST_REQUIRE_NO_THROW( DT::Options::parse_config(config) );
        const int steering = DT::fill | DT::syst;
        DP::example01("ntuple01.root", config, steering);
        BOOST_REQUIRE_THROW( DP::example02<true>("ntuple01.root",  "ntuple02.root", config, steering), wrapexcept<DE::AnomalousEvent> );
        BOOST_REQUIRE_THROW( DP::example01<true>("ntuple01.root",                   config, steering), wrapexcept<DE::BadInput      > );

        fs::remove("inexistent.out");
        fs::remove("ntuple01.root");
    }

BOOST_AUTO_TEST_SUITE_END()

#endif
