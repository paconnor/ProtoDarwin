#!/usr/bin/env zsh
set -e

submit example01 ntuple01 -c example.info -f -j 2
submit example02 ntuple01 ntuple02 -c example.info -f -j 2
submit example03 ntuple02 ntuple03 -f -s -j 2
submit example04 ntuple03 hists04 -j2
submit getMetaInfo hists04 myMetaInfo
