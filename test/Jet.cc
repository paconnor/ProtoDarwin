#ifndef DOXYGEN_SHOULD_SKIP_THIS

// Darwin
#include <Jet.h>

// BOOST
#define BOOST_TEST_MODULE Jet
#include <boost/test/included/unit_test.hpp>

// STD
#include <iostream>

using namespace Darwin::Physics;
using namespace std;

namespace utf = boost::unit_test;

BOOST_AUTO_TEST_CASE( genlevel )
{
    GenJet jet;
    BOOST_TEST( jet.scales.size() == 1 );
    BOOST_TEST( jet.weights.size() == 1 );
    BOOST_TEST( jet.nBmesons == -1 );
    BOOST_TEST( jet.nDmesons == -1 );
    BOOST_TEST( jet.partonFlavour == 0 );
    cout << jet << endl;
}

BOOST_AUTO_TEST_CASE( reclevel, * utf::tolerance(0.00001 ) )
{
    RecJet jet;
    BOOST_TEST( jet.scales.size() == 1 );
    BOOST_TEST( jet.weights.size() == 1 );
    BOOST_TEST( jet.nBmesons == -1 );
    BOOST_TEST( jet.nDmesons == -1 );
    BOOST_TEST( jet.partonFlavour == 0 );
    BOOST_TEST( jet.area == M_PI*pow(0.4,2) );
    cout << jet << endl;
}

#endif
