#ifndef DOXYGEN_SHOULD_SKIP_THIS

// Darwin
#include <Event.h>

// BOOST
#define BOOST_TEST_MODULE Event
#include <boost/test/included/unit_test.hpp>

// STD
#include <iostream>

using namespace Darwin::Physics;
using namespace std;

BOOST_AUTO_TEST_CASE( genevents )
{
    GenEvent event;
    BOOST_TEST( event.weights.front() == 1 );
    BOOST_TEST( event.weights.size () == (size_t) 1 );
    BOOST_TEST( event.hard_scale == 0 );

    event.weights.front() = 42;

    BOOST_TEST( event.weights.front() == 42 );

    event.clear();
    BOOST_TEST( event.weights.front() == 1 );
    BOOST_TEST( event.weights.size () == (size_t) 1 );
    BOOST_TEST( event.hard_scale == 0 );

    cout << event << endl;
}

BOOST_AUTO_TEST_CASE( recevents )
{
    RecEvent event;
    BOOST_TEST( event.weights.front() == 1 );
    BOOST_TEST( event.weights.size () == (size_t) 1 );
    BOOST_TEST( event.fill == 0 );
    BOOST_TEST( event.runNo == 0 );
    BOOST_TEST( event.lumi == 0 );
    BOOST_TEST( event.evtNo == (unsigned long long) 0 );

    event.weights.front() = 42;

    BOOST_TEST( event.weights.front() == 42 );

    event.clear();
    BOOST_TEST( event.weights.front() == 1 );
    BOOST_TEST( event.weights.size () == (size_t) 1 );
    BOOST_TEST( event.fill == 0 );
    BOOST_TEST( event.runNo == 0 );
    BOOST_TEST( event.lumi == 0 );
    BOOST_TEST( event.evtNo == (unsigned long long) 0 );

    cout << event << endl;
}

#endif
