#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>

#include <darwin.h>
#include <Event.h>
#include <Jet.h>

#include <boost/property_tree/ptree.hpp>      

#include <TFile.h>
#include <TH1.h>
#include <TTree.h>

using namespace std;

namespace po = boost::program_options;
namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Toy example to project a n-tuple.
void example04 (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
                const fs::path& output, //!< output ROOT file (histograms)
                const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
                const int steering, //!< parameters obtained from explicit options 
                const DT::Slice slice = {1,0} //!< number and index of slice
                )
{
    cout << __func__ << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    set<Variation> systs {nominal};
    if (steering & DT::syst)
        systs.merge(metainfo.GetVars(RecJet::ScaleVar));

    auto recEvent =        flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto genEvent = isMC ? flow.GetBranchReadOnly<GenEvent>("genEvent") : nullptr;
    auto recJets =        flow.GetBranchReadOnly<vector<RecJet>>("recJets");
    auto genJets = isMC ? flow.GetBranchReadOnly<vector<GenJet>>("genJets") : nullptr;

    map<Variation, unique_ptr<TH1D>> vars_hrecs;
    for (auto&& syst: systs) {
        string hname = "hrec";
        hname += syst.Name();
        vars_hrecs[syst] = make_unique<TH1D>(hname.c_str(), ";p_{T};N", 111, 0, 110);
    }
    auto hgen = make_unique<TH1D>("hgen", ";p_{T};N", 111, 0, 110);

    // event loop
    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[maybe_unused]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        double w = recEvent->weights.front();
        if (isMC) w *= genEvent->weights.front();
        for (const auto& [v, hrec]: vars_hrecs)
        for (const auto& recJet: *recJets) {
            hrec->Fill(recJet.CorrPt(v), w);
            cout << "recJet: " << recJets << " " << recJet.CorrPt(v) << endl;
        }

        if (!isMC) continue;

        if (recJets->size() != genJets->size()) cout << red;

        w = genEvent->weights.front();
        for (const auto& genJet: *genJets) {
            hgen->Fill(genJet.CorrPt(), w);
            cout << "genJet: " << genJets << " " << genJet.CorrPt() << endl;
        }
        cout << def;
        // here we do *NOT* fill the tree
    }

    metainfo.Set<bool>("git", "complete", true);

    fOut->cd();
    for (const auto& [_, hrec]: vars_hrecs)
        hrec->Write();
    hgen->Write();

    cout << __func__ << " end" << endl;
}

} // end of namespace

namespace DP = Darwin::Physics;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Toy example to project a n-tuple.",
                            DT::split | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DP::example04(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
