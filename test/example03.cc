#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <filesystem>
#include <string>
#include <optional>
#include <memory>

#include <darwin.h>
#include <Jet.h>

#include <boost/property_tree/ptree.hpp>      

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Toy example to modify a n-tuple.
///
/// The template argument is only for the test of exceptions in unit tests.
template<bool DARWIN_TEST_EXCEPTIONS = false>
void example03 (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
                const fs::path& output, //!< output ROOT file (n-tuple)
                const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
                const int steering, //!< parameters obtained from explicit options 
                const DT::Slice slice = {1,0} //!< number and index of slice
                )
{
    cout << __func__ << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto tOut = flow.GetOutputTree(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    metainfo.AddVars(RecJet::ScaleVar, {"JES"});

    auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets");

    // event loop
    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[maybe_unused]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        if constexpr (DARWIN_TEST_EXCEPTIONS)
            if (*looper == 5)
                BOOST_THROW_EXCEPTION(DE::AnomalousEvent("Here is an anomally", tIn));

        for (auto& recJet: *recJets) {
            recJet.area = M_PI*pow(0.4,2);
            recJet.scales = {1.1}; // nominal
            if (steering & DT::syst) {
                recJet.scales.push_back(1.05); // JESdn
                recJet.scales.push_back(1.15); // JESup
            }
            cout << recJet << endl;
        }

        if (steering & DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << " end" << endl;
}

} // end of namespace

namespace DP = Darwin::Physics;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Toy example to modify a n-tuple (JES corrections).",
                            DT::split | DT::fill | DT::syst | DT::Friend);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DP::example03(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
