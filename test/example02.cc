#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <filesystem>
#include <string>
#include <optional>
#include <memory>

#include <darwin.h>
#include <Event.h>

#include <boost/property_tree/ptree.hpp>      

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace Darwin::Physics {

////////////////////////////////////////////////////////////////////////////////
/// Toy example to modify a n-tuple.
///
/// The template argument is only for the test of exceptions in unit tests.
template<bool DARWIN_TEST_EXCEPTIONS = false>
void example02 (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
                const fs::path& output, //!< output ROOT file (n-tuple)
                const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
                const int steering, //!< parameters obtained from explicit options 
                const DT::Slice slice = {1,0} //!< number and index of slice
                )
{
    cout << __func__ << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto tOut = flow.GetOutputTree(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);

    auto recEvent = flow.GetBranchReadWrite<RecEvent>("recEvent");

    const auto& corrections = config.get_child("corrections");
    metainfo.Set<string>("corrections", "filters", corrections.get<string>("filters"));

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[maybe_unused]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        if constexpr (DARWIN_TEST_EXCEPTIONS)
            if (*looper == 5)
                BOOST_THROW_EXCEPTION(DE::AnomalousEvent("Here is an anomally", tIn));

        recEvent->weights.front() *= (*looper % 2) ? 1.1 : 0.9;
        // \todo Rather apply the filter given from the command line

        if (steering & DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << " end" << endl;
}

} // end of namespace

namespace DP = Darwin::Physics;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Toy example to modify a n-tuple (filter application).",
                            DT::config | DT::split | DT::fill | DT::Friend);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("filters", "corrections.filters",
                              "location of list of  filters to apply");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DP::example02(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
