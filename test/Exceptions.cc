#ifndef DOXYGEN_SHOULD_SKIP_THIS

// POSIX
#include <unistd.h>

// BOOST
#define BOOST_TEST_MODULE Exceptions
#include <boost/test/included/unit_test.hpp>

// Darwin
#include <exceptions.h>

using namespace Darwin::Exceptions;
using namespace std;

namespace utf = boost::unit_test;

#ifdef F_GETPIPE_SZ  // Only available on Linux
////////////////////////////////////////////////////////////////////////////////
/// Retrieves the default pipe size on this system (usually 65kB).
int GetPipeSize()
{
    // Create a pipe
    int pipefd[2];
    if (pipe(pipefd) != 0) return 0;  // Cannot create pipes?!

    // Get its size
    int size = fcntl(pipefd[0], F_GETPIPE_SZ);

    // Cleanup
    close(pipefd[0]);
    close(pipefd[1]);

    return size;
}
#endif // F_GETPIPE_SZ

////////////////////////////////////////////////////////////////////////////////
/// Write N bytes to stdout, optionally flushing it.
void WriteN(int N, bool flush)
{
    if (N > 0) printf("%*s", N, "*");
    if (flush) fflush(stdout);
}

////////////////////////////////////////////////////////////////////////////////
/// Tests \ref intercept_printf.
BOOST_AUTO_TEST_CASE( intercept, * utf::timeout(2) )
{
    // Normal case
    BOOST_TEST( intercept_printf([] { WriteN(1, true); }) == "*" );

    // Normal case, without flushing stdout before
    WriteN(1, false); // Add some garbage to the stdout buffer
    BOOST_TEST( intercept_printf([] { WriteN(1, true); }) == "*" );

    // The following tests require knowledge of system limits to avoid
    // deadlocks. The required fcntl flag, F_GETPIPE_SZ, is a Linux extension.
#ifdef F_GETPIPE_SZ
    const int maxSize = GetPipeSize();
    BOOST_REQUIRE( maxSize > 1024 ); // Make sure we have some room

    // This must not deadlock
    BOOST_TEST(
        maxSize >= intercept_printf([=] { WriteN(maxSize, true); }).size());

    // This would deadlock
    //intercept_printf([=] { WriteN(maxSize + 1, true); });

    // Long outputs get truncated
    if (maxSize >= 17'000) {
        BOOST_TEST(
            intercept_printf([] { WriteN(17'000, true); }).size() == 16'383 );
    }

    // Leave some garbage in the stdout buffer
    WriteN(10, false);
    // This must not deadlock despite writing a lot of data
    BOOST_TEST(
        maxSize >= intercept_printf([=] { WriteN(maxSize, true); }).size());
#endif // F_GETPIPE_SZ
}

#endif
