#!/usr/bin/env zsh
set -e
set -x

source ../setup

export PATH=$PATH:$PWD

rm -rf transcription
mkdir transcription
cp scenario transcription
cd transcription

transcribe scenario -d -v -D dag/scenario

cat dag/scenario/script
cat dag/scenario/dag

njobs=$(grep JOB dag/scenario/dag | wc -l)
((njobs==5))

nparents=$(grep PARENT dag/scenario/dag | wc -l)
((nparents==4))

ls dag/scenario
